package jga.environments;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EnvironmentTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void executeGenerations() {
    }

    @Test
    void executeTimeLimit() {
    }

    @Test
    void feedInitialPopulation() {
    }

    @Test
    void execute() {
    }

    @Test
    void getScoring() {
    }

    @Test
    void getSelection() {
    }

    @Test
    void getCrossover() {
    }

    @Test
    void getMutation() {
    }

    @Test
    void isSolutionFoundFlag() {
    }

    @Test
    void setSolutionFoundFlag() {
    }

    @Test
    void getResultActor() {
    }

    @Test
    void setResultActor() {
    }

    @Test
    void getGen() {
    }
}