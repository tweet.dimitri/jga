package jga.solutions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolutionTest {

    private Solution<Double> solution;

    @BeforeEach
    void setUp() {
        solution = new Solution<>(1.5d, 10, 0.1d);
    }

    @Test
    void getSolution() {
        assertEquals((Double) 1.5d, solution.getSolution());
    }

    @Test
    void getTime() {
        assertEquals(10, solution.getTime());
    }

    @Test
    void getFitness() {
        assertEquals(0.1d, solution.getFitness());
    }
}