package jga.populations;

import org.junit.jupiter.api.Test;

public abstract class PopulationTest {

    @Test
    public abstract void toIndividualPopulation();

    @Test
    public abstract void containsIndividualTest();
}
