package jga.populations;

import jga.individuals.Individual;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class SetIndividualPopulationTest extends PopulationTest {

    @Test
    @Override
    public void toIndividualPopulation() {
        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        assertEquals(individuals, individuals.toIndividualPopulation());
    }

    @Test
    @Override
    public void containsIndividualTest() {
        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        assertTrue(individuals.containsIndividual(individual));
    }
}