package jga.populations;

import jga.individuals.Individual;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class MultiIndividualPopulationTest extends PopulationTest {

    @Test
    @Override
    public void toIndividualPopulation() {
        MultiIndividualPopulation individualPopulations = new MultiIndividualPopulation();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        individualPopulations.add(individuals);

        assertEquals(individuals, individualPopulations.toIndividualPopulation());
    }

    @Test
    @Override
    public void containsIndividualTest() {
        MultiIndividualPopulation individualPopulations = new MultiIndividualPopulation();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        individualPopulations.add(individuals);

        assertTrue(individualPopulations.containsIndividual(individual));
    }
}