package jga.populations;

import jga.individuals.Individual;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class MultiIndividualMultiPopulationTest extends PopulationTest {

    @Test
    @Override
    public void toIndividualPopulation() {
        MultiIndividualMultiPopulation islands = new MultiIndividualMultiPopulation();

        MultiIndividualPopulation familyPopulation = new MultiIndividualPopulation();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        familyPopulation.add(individuals);

        islands.add(familyPopulation);

        assertEquals(individuals, islands.toIndividualPopulation());
    }

    @Test
    @Override
    public void containsIndividualTest() {
        MultiIndividualMultiPopulation islands = new MultiIndividualMultiPopulation();

        MultiIndividualPopulation familyPopulation = new MultiIndividualPopulation();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);

        individuals.add(individual);

        familyPopulation.add(individuals);

        islands.add(familyPopulation);

        assertTrue(islands.containsIndividual(individual));
    }
}