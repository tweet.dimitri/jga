package jga.operators.scoring;

import jga.operators.scoring.comparing.Comparing;

import static org.mockito.Mockito.mock;

public class ExponentialScoringTest extends ScoringTest {

    public ExponentialScoringTest() {
        Comparing compare = mock(Comparing.class);
        ExponentialScoring scoring = new ExponentialScoring(compare);
        setScoring(scoring);
    }
}
