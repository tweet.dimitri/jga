package jga.operators.scoring;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class ScoringTest {

    private Scoring scoring;

    @Test
    public void correctScoringSingle() {
        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual1 = mock(Individual.class);
        Individual individual2 = mock(Individual.class);

        when(individual1.getScore()).thenReturn(1);
        when(individual2.getScore()).thenReturn(0);

        individuals.add(individual1);
        individuals.add(individual2);

        scoring.setScoresAndSort(individuals);

        assertEquals(individual2, individuals.get(0));

    }

    public Scoring getScoring() {
        return scoring;
    }

    public void setScoring(Scoring scoring) {
        this.scoring = scoring;
    }
}
