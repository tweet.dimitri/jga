package jga.operators.scoring;


import jga.operators.scoring.comparing.Comparing;

import static org.mockito.Mockito.mock;

public class RankScoringTest extends ScoringTest {

    public RankScoringTest() {
        Comparing compare = mock(Comparing.class);
        RankScoring scoring = new RankScoring(compare);
        setScoring(scoring);
    }
}
