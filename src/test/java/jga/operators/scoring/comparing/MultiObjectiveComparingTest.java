package jga.operators.scoring.comparing;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class MultiObjectiveComparingTest {

    @Test
    public void correctScoring() {
        MultiObjectiveComparing compare = new MultiObjectiveComparing();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual1 = mock(Individual.class);
        Individual individual2 = mock(Individual.class);

        when(individual1.getFitness()).thenReturn(new double[] {0.1, 0.3});
        when(individual2.getFitness()).thenReturn(new double[] {0.3, 0.4});

        individuals.add(individual1);
        individuals.add(individual2);

        compare.compare(individuals);

        verify(individual1).setScore(1);
        verify(individual2).setScore(0);
    }

    @Test
    public void correctScoringNotComparable() {
        MultiObjectiveComparing compare = new MultiObjectiveComparing();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual1 = mock(Individual.class);
        Individual individual2 = mock(Individual.class);

        when(individual1.getFitness()).thenReturn(new double[] {0.1, 0.3});
        when(individual2.getFitness()).thenReturn(new double[] {0.3, 0.1});

        individuals.add(individual1);
        individuals.add(individual2);

        compare.compare(individuals);

        verify(individual1).setScore(0);
        verify(individual2).setScore(0);
    }
}
