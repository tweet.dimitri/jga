package jga.operators.scoring.comparing;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class SingleObjectiveComparingTest {

    @Test
    public void correctScoringSingle() {
        SingleObjectiveComparing compare = new SingleObjectiveComparing();

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual1 = mock(Individual.class);
        Individual individual2 = mock(Individual.class);

        when(individual1.getFitness()).thenReturn(new double[] {0.1});
        when(individual2.getFitness()).thenReturn(new double[] {0.3});

        individuals.add(individual1);
        individuals.add(individual2);

        compare.compare(individuals);

        verify(individual1).setScore(1);
        verify(individual2).setScore(0);
    }
}
