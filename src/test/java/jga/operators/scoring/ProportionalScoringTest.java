package jga.operators.scoring;

import jga.operators.scoring.comparing.Comparing;

import static org.mockito.Mockito.mock;

public class ProportionalScoringTest extends ScoringTest {

    public ProportionalScoringTest() {
        Comparing compare = mock(Comparing.class);
        ProportionalScoring scoring = new ProportionalScoring(compare);
        setScoring(scoring);
    }
}
