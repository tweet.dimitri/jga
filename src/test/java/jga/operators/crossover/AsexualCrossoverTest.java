package jga.operators.crossover;

import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AsexualCrossoverTest extends CrossoverTest {

    @Test
    @Override
    public void breedCorrectAmount() {
        AsexualCrossover crossover = new AsexualCrossover();

        IndividualPopulation population = crossover.breed(getPopulation(), getWantedSize());
        assertEquals(getWantedSize(), population.size() + getPopulation().size());
    }

    @Test
    @Override
    public void breed() {

    }
}
