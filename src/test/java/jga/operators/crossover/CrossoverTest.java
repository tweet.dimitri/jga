package jga.operators.crossover;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class CrossoverTest {

    private IndividualPopulation population;
    private int initialSize;
    private int wantedSize;

    public CrossoverTest() {
        population = new IndividualPopulation();
        initialSize = 10;
        wantedSize = 21;

        Individual individual = mock(Individual.class);
        when(individual.getDNALength()).thenReturn(6);

        Individual individual2 = mock(Individual.class);
        when(individual2.getDNALength()).thenReturn(4);

        for (int i = 0; i < initialSize / 2; i++) {
            population.add(individual);
            population.add(individual2);
        }
    }

    @Test
    public abstract void breedCorrectAmount();

    @Test
    public abstract void breed();

    public IndividualPopulation getPopulation() {
        return population;
    }

    public void setPopulation(IndividualPopulation population) {
        this.population = population;
    }

    public int getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(int initialSize) {
        this.initialSize = initialSize;
    }

    public int getWantedSize() {
        return wantedSize;
    }

    public void setWantedSize(int wantedSize) {
        this.wantedSize = wantedSize;
    }
}
