package jga.operators.crossover;

import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleWithLayBackCrossoverTest extends CrossoverTest {

    @Test
    @Override
    public void breedCorrectAmount() {
        SimpleWithLayBackCrossover crossover = new SimpleWithLayBackCrossover();

        IndividualPopulation population = crossover.breed(getPopulation(), getWantedSize());
        assertEquals(getWantedSize(), population.size() + getPopulation().size());
    }

    @Test
    @Override
    public void breed() {

    }
}
