package jga.operators.mutation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AdaptiveMutationTest extends MutationTest {

    public AdaptiveMutationTest() {
        super(new AdaptiveMutation(1, 0.25, 0.25, 0.25));
    }

    @Test
    void mutate() {

    }

    @Test
    void getSetMutationsPerIndividual() {
        double original = ((AdaptiveMutation) getMutation()).getMutationsPerIndividual();
        ((AdaptiveMutation) getMutation()).setMutationsPerIndividual(20);

        assertEquals(20, ((AdaptiveMutation) getMutation()).getMutationsPerIndividual());

        ((AdaptiveMutation) getMutation()).setMutationsPerIndividual(original);
    }
}