package jga.operators.mutation;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

abstract class MutationTest {

    private Mutation mutation;
    private Collection invocations;

    public MutationTest(Mutation mutation) {
        this.mutation = mutation;
        this.mutation.setMutationRate(0.01);
        this.mutation.setAddMutationChance(0.25d);
        this.mutation.setRemoveMutationChance(0.25d);
        this.mutation.setPointMutationChance(0.25d);
        this.mutation.setDeltaMutationChance(0.25d);

        IndividualPopulation individuals = new IndividualPopulation();

        Individual individual = mock(Individual.class);
        when(individual.getDNALength()).thenReturn(100);

        individuals.add(individual);

        for (int i = 0; i < 1000; i++) {
            this.mutation.mutate(individuals);
        }

        invocations = Mockito.mockingDetails(individual).getInvocations();
    }

    @Test
    public void constructorThrows() {
        Executable executable = () -> new SimpleMutation(0.1, 0.3, 0.5, 0.4);
        assertThrows(IllegalArgumentException.class, executable);
    }

    @Test
    void mutateChance() {
        Iterator iterator = invocations.iterator();

        int count = 0;
        while (iterator.hasNext()) {
            if (iterator.next().toString().contains("Mutation")) {
                count++;
            }
        }

        assertEquals(1000, count, 100);
    }

    @Test
    void mutateAddChance() {
        Iterator iterator = invocations.iterator();

        int count = 0;
        while (iterator.hasNext()) {
            if (iterator.next().toString().contains("addMutation")) {
                count++;
            }
        }

        assertEquals(250, count, 50);
    }

    @Test
    void mutateRemoveChance() {
        Iterator iterator = invocations.iterator();

        int count = 0;
        while (iterator.hasNext()) {
            if (iterator.next().toString().contains("removeMutation")) {
                count++;
            }
        }

        assertEquals(250, count, 50);
    }

    @Test
    void mutatePointChance() {
        Iterator iterator = invocations.iterator();

        int count = 0;
        while (iterator.hasNext()) {
            if (iterator.next().toString().contains("pointMutation")) {
                count++;
            }
        }

        assertEquals(250, count, 50);
    }

    @Test
    void mutateDeltaChance() {
        Iterator iterator = invocations.iterator();

        int count = 0;
        while (iterator.hasNext()) {
            if (iterator.next().toString().contains("deltaMutation")) {
                count++;
            }
        }

        assertEquals(250, count, 50);
    }

    @Test
    void getSetMutationRate() {
        final double chance = 0.423;
        mutation.setMutationRate(chance);
        assertEquals(chance, mutation.getMutationRate());
    }

    @Test
    void getSetAddMutationChance() {
        final double chance = 0.423;
        mutation.setAddMutationChance(chance);
        assertEquals(chance, mutation.getAddMutationChance());
    }

    @Test
    void getSetRemoveMutationChance() {
        final double chance = 0.423;
        mutation.setRemoveMutationChance(chance);
        assertEquals(chance, mutation.getRemoveMutationChance());
    }

    @Test
    void getSetPointMutationChance() {
        final double chance = 0.423;
        mutation.setPointMutationChance(chance);
        assertEquals(chance, mutation.getPointMutationChance());
    }

    @Test
    void getSetDeltaMutationChance() {
        final double chance = 0.423;
        mutation.setDeltaMutationChance(chance);
        assertEquals(chance, mutation.getDeltaMutationChance());
    }

    public Mutation getMutation() {
        return mutation;
    }

    public void setMutation(Mutation mutation) {
        this.mutation = mutation;
    }
}