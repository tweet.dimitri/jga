package jga.individuals;

import jga.datastructures.DataStructure;
import jga.problems.Problem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GrammarIndividualTest {

    private GrammarIndividual<Integer, List<Integer>> individual;

    @BeforeEach
    void setUp() {
        Problem problem = mock(Problem.class);
        DataStructure<Integer> structure = mock(DataStructure.class);

        when(structure.getRandom(any())).thenReturn((new Random()).nextInt(10));
        when(structure.getCloseDelta(any(), any())).thenReturn((new Random()).nextInt(10));

        int[] sizes = new int[]{1, 1};

        GrammarIndividual<Integer, List<Integer>> parent = new GrammarIndividual<>(problem, structure, sizes, true);

        List<List<Integer>> DNA = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            DNA.add(new ArrayList<>());
            for (int j = 0; j < 3; j++) {
                DNA.get(i).add(1);
            }
        }

        this.individual = new GrammarIndividual<>(parent, DNA);
    }

    @Test
    void generateDNA() {
    }

    @Test
    void crossover() {
    }

    @Test
    void addMutationBefore() {

        individual.addMutation(3);

        assertEquals(4, individual.getDNA().get(0).size());
    }

    @Test
    void addMutationAfter() {

        individual.addMutation(4);

        assertEquals(4, individual.getDNA().get(1).size());

    }

    @Test
    void addMutationBeforeNoMoreDNA() {
        individual.addMutation(6);

        assertEquals(4, individual.getDNA().get(1).size());
    }

    @Test
    void addMutationAfterNoMoreDNA() {
        int before = individual.getDNALength();
        // This should throw an exception

//        individual.addMutation(7);

        assertEquals(before, individual.getDNALength());
    }


    @Test
    void removeMutation() {
    }

    @Test
    void pointMutation() {
    }

    @Test
    void deltaMutation() {
    }

    @Test
    void calculateSize() {
    }

    @Test
    void copy() {
    }
}
