//package system;
//
//import jga.environments.FamilyEnvironment;
//import jga.problems.Problem;
//import jga.individuals.single.SCBinaryIndividual;
//import jga.operators.crossover.SimpleWithLayBackCrossover;
//import jga.operators.mutation.SimpleMutation;
//import jga.operators.scoring.RankScoring;
//import jga.operators.scoring.comparing.SingleObjectiveComparing;
//import jga.operators.selection.TournamentSelection;
//import jga.populations.MultiIndividualPopulation;
//import jga.populations.IndividualPopulation;
//import org.junit.jupiter.api.Test;
//
//public class FamilyInstanceTest {
//
//    @Test
//    public void instanceTest() {
//        final int populationSize = 1000;
//
//        SingleObjectiveComparing compare = new SingleObjectiveComparing();
////        MultiObjectiveComparing compare = new MultiObjectiveComparing();
//
//        RankScoring scoring = new RankScoring(compare);
//
////        AsexualCrossover crossover = new AsexualCrossover();
//        SimpleWithLayBackCrossover crossover = new SimpleWithLayBackCrossover();
////        SimpleWithoutLayBackCrossover crossover = new SimpleWithoutLayBackCrossover();
//
////        SimpleElitismSelection selection = new SimpleElitismSelection();
////        ChanceElitismWithLayBackSelection selection = new ChanceElitismWithLayBackSelection();
////        ChanceElitismWithoutLayBackSelection selection = new ChanceElitismWithoutLayBackSelection();
////        RouletteWheelWithLayBackSelection selection = new RouletteWheelWithLayBackSelection();
////        RouletteWheelWithoutLayBackSelection selection = new RouletteWheelWithoutLayBackSelection();
//        TournamentSelection selection = new TournamentSelection();
//
//        SimpleMutation simpleMutation = new SimpleMutation(0.7, 0.25, 0.25, 0.25);
//
//        FamilyEnvironment<boolean[][]> environment = new FamilyEnvironment<>(scoring, selection, crossover, simpleMutation);
//
//        int familySize = 4;
//        int generations = 100;
//
//        int familyAmount = populationSize / familySize;
//
//        // single objective problem
//        Problem<boolean[], boolean[][]> problem = (booleans, object) -> {
//            double wrongCount = 0;
//            int count = 0;
//            for (int i = 0; i < object.length; i++) {
//                for (int j = 0; j < object[i].length; j++) {
//                    if (object[i][j] != booleans[count]) {
//                        wrongCount++;
//                    }
//                    count++;
//                }
//            }
//
//            return new double[] {wrongCount / count};
//        };
//
//        boolean[][] data = new boolean[][] {
//                //  0   1    2    3    4     5
//                {true, false, true, false, true, false},
//                {true, true, false, true, false, false},
//                {true, false, false, true, false, true},
//                {false, true, true, false, true, false},
//                {true, false, false, true, true, false}
//        };
//
//        int totalDataSize = data.length * data[0].length;
//
//        MultiIndividualPopulation families = new MultiIndividualPopulation();
//
//        for (int i = 0; i < familyAmount; i++) {
//            IndividualPopulation family = new IndividualPopulation();
//
//            for (int j = 0; j < familySize; j++) {
//                SCBinaryIndividual<boolean[][]> individual = new SCBinaryIndividual<>(problem, totalDataSize);
//                family.add(individual);
//            }
//
//            families.add(family);
//        }
//
//        MultiIndividualPopulation populations = environment.executeGenerations(data, families, generations);
//
//        IndividualPopulation individuals = new IndividualPopulation();
//        for (IndividualPopulation population : populations) {
//            individuals.addAll(population);
//        }
//
//        for (int i = 0; i < individuals.size(); i++) {
//            individuals.get(i).processData(data);
//        }
//
//        compare.compare(individuals);
//
////        SCBinaryIndividual<boolean[]> best = (SCBinaryIndividual<boolean[]>) individuals.get(0);
//    }
//}
