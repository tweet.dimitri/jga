//package system;
//
//import jga.environments.SimpleEnvironment;
//import jga.problems.Problem;
//import jga.individuals.single.SCBinaryIndividual;
//import jga.operators.crossover.AsexualCrossover;
//import jga.operators.mutation.SimpleMutation;
//import jga.operators.scoring.RankScoring;
//import jga.operators.scoring.comparing.SingleObjectiveComparing;
//import jga.operators.selection.ChanceElitismWithLayBackSelection;
//import jga.populations.IndividualPopulation;
//import org.junit.jupiter.api.Test;
//
//public class SimpleInstanceTest {
//
//    @Test
//    public void instanceTest() {
//        final int populationSize = 1000;
//
//        SingleObjectiveComparing compare = new SingleObjectiveComparing();
////        MultiObjectiveComparing compare = new MultiObjectiveComparing();
//
//        RankScoring scoring = new RankScoring(compare);
//
//        AsexualCrossover crossover = new AsexualCrossover();
////        SimpleWithLayBackCrossover crossover = new SimpleWithLayBackCrossover(populationSize);
////        SimpleWithoutLayBackCrossover crossover = new SimpleWithoutLayBackCrossover(populationSize);
//
////        SimpleElitismSelection selection = new SimpleElitismSelection(populationSize / 2);
//        ChanceElitismWithLayBackSelection selection = new ChanceElitismWithLayBackSelection();
////        ChanceElitismWithoutLayBackSelection selection = new ChanceElitismWithoutLayBackSelection(populationSize / 2);
////        RouletteWheelWithLayBackSelection selection = new RouletteWheelWithLayBackSelection(populationSize / 2);
////        RouletteWheelWithoutLayBackSelection selection = new RouletteWheelWithoutLayBackSelection(populationSize / 2);
////        TournamentSelection selection = new TournamentSelection(populationSize / 2);
//
//        SimpleMutation simpleMutation = new SimpleMutation(1, 0.25, 0.25, 0.25);
//
//        SimpleEnvironment<boolean[][]> environment = new SimpleEnvironment<>(scoring, selection, crossover, simpleMutation);
//
//        int generations = 1000;
//
//        // single objective problem
//        Problem<boolean[], boolean[][]> problem = (booleans, object) -> {
//            double wrongCount = 0;
//            int count = 0;
//            for (int i = 0; i < object.length; i++) {
//                for (int j = 0; j < object[i].length; j++) {
//                    if (object[i][j] != booleans[count]) {
//                        wrongCount++;
//                    }
//                    count++;
//                }
//            }
//
//            return new double[] {wrongCount / count};
//        };
//
//        boolean[][] data = new boolean[][] {
//                //  0   1    2    3    4     5
//                {true, false, true, false, true, false},
//                {true, true, false, true, false, false},
//                {true, false, false, true, false, true},
//                {false, true, true, false, true, false},
//                {true, false, false, true, true, false}
//        };
//
//        int totalDataSize = data.length * data[0].length;
//
//        IndividualPopulation individuals = new IndividualPopulation();
//
//        for (int i = 0; i < populationSize; i++) {
//            SCBinaryIndividual<boolean[][]> individual = new SCBinaryIndividual<>(problem, totalDataSize);
//            individuals.add(individual);
//        }
//
//        individuals = environment.executeGenerations(data, individuals, generations);
//
//        for ( individual : individuals) {
//            individual.processData(data);
//        }
//
//        compare.compare(individuals);
//
////        SCBinaryIndividual<boolean[]> best = (SCBinaryIndividual<boolean[]>) individuals.get(0);
//    }
//}
