package system;

import jga.actors.Reporter;
import jga.datastructures.DoubleRange;
import jga.environments.MultiEnvironment;
import jga.individuals.GrammarIndividual;
import jga.individuals.Individual;
import jga.operators.crossover.SimpleWithLayBackCrossover;
import jga.operators.mutation.AdaptiveMutation;
import jga.operators.scoring.RankScoring;
import jga.operators.scoring.comparing.SingleObjectiveComparing;
import jga.operators.selection.SimpleElitismSelection;
import jga.populations.MultiIndividualPopulation;
import jga.populations.IndividualPopulation;
import jga.problems.Problem;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CoEvolutionInstanceTest {

    @Test
    public void instanceTest() {
        final int populationSize = 1000;

        SingleObjectiveComparing compare = new SingleObjectiveComparing();
//        MultiObjectiveComparing compare = new MultiObjectiveComparing();

        RankScoring scoring = new RankScoring(compare);

//        AsexualCrossover crossover = new AsexualCrossover();
        SimpleWithLayBackCrossover crossover = new SimpleWithLayBackCrossover();
//        SimpleWithoutLayBackCrossover crossover = new SimpleWithoutLayBackCrossover();

        SimpleElitismSelection selection = new SimpleElitismSelection();
//        ChanceElitismWithLayBackSelection selection = new ChanceElitismWithLayBackSelection();
//        ChanceElitismWithoutLayBackSelection selection = new ChanceElitismWithoutLayBackSelection();
//        RouletteWheelWithLayBackSelection selection = new RouletteWheelWithLayBackSelection();
//        RouletteWheelWithoutLayBackSelection selection = new RouletteWheelWithoutLayBackSelection();
//        TournamentSelection selection = new TournamentSelection();

        AdaptiveMutation simpleMutation = new AdaptiveMutation(1, 0.25, 0.25, 0.25);

        MultiEnvironment<double[][]> environment = new MultiEnvironment<>(scoring, selection, crossover, simpleMutation);

        int islandSize = 20;
        int generations = 1000;

        int islandAmount = populationSize / islandSize;

        // single objective problem
        Problem<List<List<Double>>, double[][]> problem = (fitness, dna, object) -> {
            double correct = 0;
            int count = 0;
            for (int i = 0; i < object.length; i++) {
                for (int j = 0; j < object[i].length; j++) {
                    if (object[i][j] + 0.01 > dna.get(i).get(j) && object[i][j] - 0.01 < dna.get(i).get(j)) {
                        correct++;
                    }

                    count++;
                }
            }

            return new double[] {(correct / count)};
        };

        double[][] data = new double[][] {
                //  0   1    2    3    4     5
                {0.1, 0.2, 0.892, 0.2, 0.892, 0.2},
                {0.892, 0.892, 0.2, 0.892, 0.2, 0.2},
                {0.892, 0.2, 0.2, 0.892, 0.2, 0.892},
                {0.2, 0.892, 0.892, 0.2, 0.892, 0.2},
                {0.892, 0.2, 0.2, 0.892, 0.892, 0.2}
        };

        MultiIndividualPopulation islands = new MultiIndividualPopulation();

        DoubleRange doubleRange = new DoubleRange(0.5, 0.001);

        int[] size = new int[]{6, 6, 6, 6, 6};

        for (int i = 0; i < islandAmount; i++) {
            IndividualPopulation island = new IndividualPopulation();

            for (int j = 0; j < islandSize; j++) {
                GrammarIndividual<Double, double[][]> individual = new GrammarIndividual<>(problem, doubleRange, size, false);
                island.add(individual);
            }

            islands.add(island);
        }

        Reporter reporter = new Reporter(environment, 1) {
            @Override
            public void reportFunction() {
                System.out.println(getEnvironment().getGen() + " : " + getEnvironment().getStage());
            }
        };


        MultiIndividualPopulation populations = (MultiIndividualPopulation) environment.executeGenerations(data, islands, generations);

        reporter.killThread();

        IndividualPopulation individuals = new IndividualPopulation();
        for (IndividualPopulation population : populations) {
            individuals.addAll(population);
        }

        for (Individual individual : individuals) {
            individual.processData(data);
        }

        scoring.setScoresAndSort(individuals);


        for (int i = 0; i < ((List<List<Double>>)individuals.get(0).getDNA()).size(); i++) {
            System.out.println(((List<List<Double>>)individuals.get(0).getDNA()).get(i).toString());
        }
    }
}
