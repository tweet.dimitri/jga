package system;

import examples.actors.MultiIndividualPopulationMigrationActor;
import examples.problems.LinearVocabularyCheck;
import jga.actors.Actor;
import jga.actors.Reporter;
import jga.datastructures.Grammar;
import jga.environments.MultiEnvironment;
import jga.individuals.GrammarIndividual;
import jga.individuals.Individual;
import jga.operators.crossover.AsexualCrossover;
import jga.operators.mutation.AdaptiveMutation;
import jga.operators.scoring.RankScoring;
import jga.operators.scoring.comparing.SingleObjectiveComparing;
import jga.operators.selection.SimpleElitismSelection;
import jga.populations.IndividualPopulation;
import jga.populations.MultiIndividualPopulation;
import jga.problems.Problem;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HierarchyPathCompareTest {
    @Test
    public void instanceTest() {
        final int populationSize = 100;

        SingleObjectiveComparing compare = new SingleObjectiveComparing();
        compare.setHierarchyPathIndex(0);

        RankScoring scoring = new RankScoring(compare);
        AsexualCrossover crossover = new AsexualCrossover();
        SimpleElitismSelection selection = new SimpleElitismSelection();
        AdaptiveMutation simpleMutation = new AdaptiveMutation(5, 0.25, 0.25, 0.25);

        MultiEnvironment<List<String>> environment = new MultiEnvironment<>(scoring, selection, crossover, simpleMutation);

        MultiIndividualPopulationMigrationActor migrationActor = new MultiIndividualPopulationMigrationActor(0.5);

        environment.addActor(migrationActor);

        Actor actor = (environment1, population) -> {
            IndividualPopulation individuals = population.toIndividualPopulation();
//            environment1.getScoring().setScoresAndSort(individuals);
            Individual best = individuals.get(0);

            if (best.getFitness()[0] == 1) {
                environment1.setSolutionFoundFlag(true);
            }
        };

        environment.addActor(actor);

        LinearVocabularyCheck<String> check = new LinearVocabularyCheck<>();

        Problem<List<List<String>>, List<String>> problem = (fitness, chromosome, target) -> {
            double distance = check.compare(target, chromosome.get(0));

            double[] fitnessNew = new double[populationSize];

            for (int i = 0; i < fitnessNew.length; i++) {
                fitnessNew[i] =  1d / (distance + 1d);
            }

            return fitnessNew;
        };

        List<String> words = new ArrayList<>();
        words.add("hello");
        words.add("world");
        words.add("how");
        words.add("are");
        words.add("you");
        words.add("who");
        words.add("have");
        words.add("been");

        words.add("I");
        words.add("ape");
        words.add("nut");
        words.add("me");
        words.add("we");
        words.add("research");
        words.add("bro");
        words.add("noob");

        String space = " ";

        List<String> punctuation = new ArrayList<>();
        punctuation.add(",");
        punctuation.add(".");
        punctuation.add("!");
        punctuation.add("?");

        Grammar<String> vocabulair = new Grammar<>(words);

        vocabulair.addBranches(punctuation, space);

        vocabulair.addBranches(space, words);

        vocabulair.addBranches(words, space);
        vocabulair.addBranches(words, punctuation);

        MultiIndividualPopulation population = new MultiIndividualPopulation();

        int[] sizes = new int[]{30};

        for (int i = 0; i < populationSize; i++) {
            IndividualPopulation individuals = new IndividualPopulation();
            for (int j = 0; j < 5; j++) {
                individuals.add(new GrammarIndividual(problem, vocabulair, sizes, true));
            }
            population.add(individuals);
        }


        List<String> data = new ArrayList<>();
        data.add("hello");
        data.add(" ");
        data.add("world");
        data.add(",");
        data.add(" ");
        data.add("how");
        data.add(" ");
        data.add("are");
        data.add(" ");
        data.add("you");
        data.add("!");


        population = (MultiIndividualPopulation) environment.executeTimeLimit(data, population, 10);

        scoring.setScoresAndSort(population.toIndividualPopulation());

        GrammarIndividual<String, List<String>> best = (GrammarIndividual<String, List<String>>) population.get(0).get(0);
        System.out.println("RESULTS");
        System.out.println("Solution found in: " + environment.getGen() + " generations");
        System.out.println(Arrays.toString(best.getDNA().toArray()));
    }
}

