package system;

import jga.actors.Reporter;
import jga.datastructures.Grammar;
import jga.environments.SimpleEnvironment;
import jga.actors.Actor;
import jga.individuals.GrammarIndividual;
import jga.individuals.Individual;
import jga.operators.scoring.comparing.SingleObjectiveComparing;
import jga.problems.Problem;
import examples.problems.LinearVocabularyCheck;
import jga.operators.crossover.AsexualCrossover;
import jga.operators.mutation.AdaptiveMutation;
import jga.operators.scoring.RankScoring;
import jga.operators.selection.SimpleElitismSelection;
import jga.populations.IndividualPopulation;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GrammarTest {

    @Test
    public void instanceTest() {
        int populationSize = 100;

        SingleObjectiveComparing compare = new SingleObjectiveComparing();
        RankScoring scoring = new RankScoring(compare);
        AsexualCrossover crossover = new AsexualCrossover();
        SimpleElitismSelection selection = new SimpleElitismSelection();
        AdaptiveMutation simpleMutation = new AdaptiveMutation(5, 0.25, 0.25, 0.25);

        SimpleEnvironment<List<String>> environment = new SimpleEnvironment<>(scoring, selection, crossover, simpleMutation);

        Actor actor = (environment1, population) -> {
            IndividualPopulation individuals = population.toIndividualPopulation();
            individuals.sort((o1, o2) -> Double.compare(
                    o2.getFitness()[0],
                    o1.getFitness()[0]));

            Individual best = individuals.get(0);
            if (best.getFitness()[0] == 1) {
                environment1.setSolutionFoundFlag(true);
            }
        };

        environment.addActor(actor);

        LinearVocabularyCheck<String> check = new LinearVocabularyCheck<>();

        Problem<List<List<String>>, List<String>> problem = (fitness, chromosome, target) -> {
            double distance = check.compare(target, chromosome.get(0));

            return new double[] { 1d / (distance + 1d)};
        };

        List<String> words = new ArrayList<>();
        words.add("hello");
        words.add("world");
        words.add("how");
        words.add("are");
        words.add("you");
        words.add("who");
        words.add("have");
        words.add("been");

        words.add("I");
        words.add("ape");
        words.add("nut");
        words.add("me");
        words.add("we");
        words.add("research");
        words.add("bro");
        words.add("noob");

        String space = " ";

        List<String> punctuation = new ArrayList<>();
        punctuation.add(",");
        punctuation.add(".");
        punctuation.add("!");
        punctuation.add("?");

        Grammar<String> vocabulair = new Grammar<>(words);

        vocabulair.addBranches(punctuation, space);

        vocabulair.addBranches(space, words);

        vocabulair.addBranches(words, space);
        vocabulair.addBranches(words, punctuation);

        IndividualPopulation population = new IndividualPopulation();

        int[] sizes = new int[]{30};

        for (int i = 0; i < populationSize; i++) {
            population.add(new GrammarIndividual(problem, vocabulair, sizes, true));
        }

        List<String> data = new ArrayList<>();
        data.add("hello");
        data.add(" ");
        data.add("world");
        data.add(",");
        data.add(" ");
        data.add("how");
        data.add(" ");
        data.add("are");
        data.add(" ");
        data.add("you");
        data.add("!");

        Reporter reporter = new Reporter(environment, 1) {
            @Override
            public void reportFunction() {
                System.out.println(getEnvironment().getGen() + " : " + getEnvironment().getStage());
            }
        };

        population = environment.executeTimeLimit(data, population, 10);

        reporter.killThread();

        for (Individual individual : population) {
            individual.processData(data);
        }

        scoring.setScoresAndSort(population);

        GrammarIndividual<String, List<String>> best = (GrammarIndividual<String, List<String>>) population.get(0);
        System.out.println("RESULTS");
        System.out.println("Solution found in: " + environment.getGen() + " generations");
        System.out.println(Arrays.toString(best.getDNA().toArray()));
    }
}
