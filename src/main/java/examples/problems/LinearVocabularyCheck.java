package examples.problems;

import java.util.List;

public class LinearVocabularyCheck<DataObject> {

    public double compare(List<DataObject> target, List<DataObject> solution) {
        double score = 0;

        for (int i = 0; i < target.size(); i++) {
            if (i >= solution.size()) {
                score += target.size() - solution.size();
                break;
            }

            if (!target.get(i).equals(solution.get(i))) {
                score += target.size() - i;
                break;
            }
        }

        score += solution.size() - target.size() > 0 ? solution.size() - target.size(): 0;

        if (score == 0) {
            System.out.println(solution);
        }
        return score;
    }

}
