package examples.actors;

import jga.actors.MigrationActor;
import jga.individuals.Individual;
import jga.populations.*;
import jga.utils.RandomUtil;

public class MultiIndividualMultiPopulationMigrationActor extends MigrationActor {

    public MultiIndividualMultiPopulationMigrationActor(double rate) {
        super(rate);
    }

    @Override
    public void migrate(MultiPopulation population) {
        if (population.size() < 2) {
            return;
        }

        MultiIndividualMultiPopulation actual = (MultiIndividualMultiPopulation) population;

        IndividualPopulation individuals = actual.toIndividualPopulation();
        int count = 0;
        for (Individual individualToMove : individuals) {
            if (RandomUtil.getInstance().nextDouble() < getRate()) {
                count++;
                int oldHomeIslandIndex = -1;
                for (int j = 0; j < actual.size(); j++) {
                    if (((Population) actual.get(j)).containsIndividual(individualToMove)) {
                        oldHomeIslandIndex = j;
                        break;
                    }
                }

                if (oldHomeIslandIndex == -1) {
                    throw new IllegalStateException("Individual has no home island!!");
                }

                int newHomeIslandIndex = RandomUtil.getInstance().nextInt(actual.size());

                while (oldHomeIslandIndex == newHomeIslandIndex) {
                    newHomeIslandIndex = RandomUtil.getInstance().nextInt(actual.size());
                }

                int famIndex = RandomUtil.getInstance().nextInt(actual.get(newHomeIslandIndex).size());
                actual.get(newHomeIslandIndex).get(famIndex).add(individualToMove.copy());
            }
        }
        actual.setMigrations(count);
    }

}
