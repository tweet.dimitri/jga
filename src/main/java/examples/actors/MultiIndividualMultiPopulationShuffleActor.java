package examples.actors;

import jga.actors.ShuffleActor;
import jga.populations.MultiIndividualMultiPopulation;
import jga.populations.MultiIndividualPopulation;
import jga.populations.MultiPopulation;

public class MultiIndividualMultiPopulationShuffleActor extends ShuffleActor {

    private MultiIndividualPopulationShuffleActor actor;

    public MultiIndividualMultiPopulationShuffleActor(MultiIndividualPopulationShuffleActor actor) {
        this.actor = actor;
    }

    @Override
    public void shuffle(MultiPopulation population) {
        if(!(population instanceof MultiIndividualPopulation)) {
            throw new IllegalStateException("Population must be of type MultiIndividualMultiPopulation!");
        }
        for (Object o : population) {
            actor.shuffle((MultiIndividualPopulation) o);
        }

    }
}
