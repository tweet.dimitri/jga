package examples.actors;

import jga.actors.MigrationActor;
import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.populations.MultiIndividualPopulation;
import jga.populations.MultiPopulation;
import jga.populations.Population;
import jga.utils.RandomUtil;

public class MultiIndividualPopulationMigrationActor extends MigrationActor {

    public MultiIndividualPopulationMigrationActor(double rate) {
        super(rate);
    }

    @Override
    public void migrate(MultiPopulation population) {
        if (population.size() < 2) {
            return;
        }

        MultiIndividualPopulation actual = (MultiIndividualPopulation) population;

        IndividualPopulation individuals = actual.toIndividualPopulation();
        int count = 0;
        for (Individual individualToMove : individuals) {
            if (RandomUtil.getInstance().nextDouble() < getRate()) {
                count++;
                int oldHomeIslandIndex = -1;
                for (int j = 0; j < actual.size(); j++) {
                    if (((Population) actual.get(j)).containsIndividual(individualToMove)) {
                        oldHomeIslandIndex = j;
                        break;
                    }
                }

                if (oldHomeIslandIndex == -1) {
                    throw new IllegalStateException("Individual has no home island!!");
                }

                int newHomeIslandIndex = RandomUtil.getInstance().nextInt(actual.size());

                while (oldHomeIslandIndex == newHomeIslandIndex) {
                    newHomeIslandIndex = RandomUtil.getInstance().nextInt(actual.size());
                }

                actual.get(newHomeIslandIndex).add(individualToMove.copy());
            }
        }
        actual.setMigrations(count);
    }
}
