package examples.actors;

import jga.actors.ShuffleActor;
import jga.individuals.Individual;
import jga.populations.MultiIndividualPopulation;
import jga.populations.IndividualPopulation;
import jga.populations.MultiPopulation;
import jga.utils.RandomUtil;

public class MultiIndividualPopulationShuffleActor extends ShuffleActor {
    @Override
    public void shuffle(MultiPopulation population) {
        MultiIndividualPopulation actual = (MultiIndividualPopulation) population;

        MultiIndividualPopulation newFamilies = new MultiIndividualPopulation();

        IndividualPopulation newFamily = new IndividualPopulation();

        while (actual.size() != 0) {
            int familyIndex = RandomUtil.getInstance().nextInt(actual.size());
            if (actual.get(familyIndex).size() < 1) {
                throw new IllegalArgumentException("Family is empty!");
            }
            int individualIndex = RandomUtil.getInstance().nextInt(actual.get(familyIndex).size());
            Individual individual = actual.get(familyIndex).remove(individualIndex);

            newFamily.add(individual);

            if (newFamily.size() == 2) {
                newFamilies.add(newFamily);
                newFamily = new IndividualPopulation();
            }

            if (actual.get(familyIndex).size() == 0) {
                actual.remove(familyIndex);
            }
        }

        actual.addAll(newFamilies);
    }
}
