package jga.solutions;

/**
 * This class implements the Solution object.
 * @param <DNA> the type of dna it holds
 */
public class Solution<DNA> {

    private DNA solution;
    private long time;
    private double fitness;

    /**
     * Constructor for a solution object.
     * @param dna the found dna solution.
     * @param time the time it took to find this solution.
     * @param fitness the fitness of the solution.
     */
    public Solution(DNA dna, long time, double fitness) {
        this.solution = dna;
        this.time = time;
        this.fitness = fitness;
    }

    /**
     * Gets the solution.
     * @return the solution
     */
    public DNA getSolution() {
        return solution;
    }

    /**
     * Gets the time it took.
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * Gets the fitness of the solution.
     * @return the fitness
     */
    public double getFitness() {
        return fitness;
    }
}
