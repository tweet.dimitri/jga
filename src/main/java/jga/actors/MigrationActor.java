package jga.actors;

import jga.environments.Environment;
import jga.populations.MultiPopulation;
import jga.populations.Population;

/**
 * This class describes a migration actor.
 */
public abstract class MigrationActor implements Actor {

    private double rate;

    /**
     * Constructor.
     *
     * @param rate the percentage of the population that must migrate
     */
    public MigrationActor(double rate) {
        this.rate = rate;
    }

    @Override
    public void act(Environment environment, Population population) {
        if (!(population instanceof MultiPopulation)) {
            throw new IllegalStateException("Migration Actor only works on MultiPopulations");
        }
        environment.setStage("Migrate");
        migrate((MultiPopulation) population);
    }

    /**
     * This method migrates some individuals to other sub populations.
     *
     * @param population the population
     */
    public abstract void migrate(MultiPopulation population);

    /**
     * This method gets the rate of migration.
     *
     * @return the migration rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * This method sets the rate of migration.
     *
     * @param rate the new value
     */
    public void setRate(double rate) {
        this.rate = rate;
    }
}
