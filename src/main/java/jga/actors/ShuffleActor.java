package jga.actors;

import jga.environments.Environment;
import jga.populations.MultiPopulation;
import jga.populations.Population;

/**
 * This class describes a migration actor.
 */
public abstract class ShuffleActor implements Actor {

    @Override
    public void act(Environment environment, Population population) {
        if (!(population instanceof MultiPopulation)) {
            throw new IllegalStateException("Migration Actor only works on MultiPopulations");
        }
        environment.setStage("Shuffle families");
        shuffle((MultiPopulation) population);
    }

    /**
     * This method shuffles individuals between the sub populations.
     *
     * @param population the population
     */
    public abstract void shuffle(MultiPopulation population);

}
