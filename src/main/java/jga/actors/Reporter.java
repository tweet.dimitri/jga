package jga.actors;

import jga.environments.Environment;

/**
 * This class describes a reporter.
 */
public abstract class Reporter {

    private Environment environment;
    private long reportingInterval;
    private boolean running;

    private long startTime;

    /**
     * Constructor.
     *
     * @param environment the environment to report on
     * @param reportingInterval the interval between reports in seconds
     */
    public Reporter(Environment environment, double reportingInterval) {
        this.environment = environment;
        this.running = true;
        final int secondsToMillis = 1000;
        this.reportingInterval = (long) (reportingInterval * secondsToMillis);
        createReporterThread();
    }

    /**
     * This method creates a reporter thread.
     * This thread will keep running until running becomes false.
     */
    public void createReporterThread() {
        Thread thread = new Thread(() -> {
            startTime = System.currentTimeMillis();
            long last = startTime;

            while (running) {
                if (System.currentTimeMillis() - last > reportingInterval) {
                    reportFunction();
                    last = System.currentTimeMillis();
                }
            }
        });
        thread.start();
    }

    /**
     * This method does the actual reporting.
     * It gets called after each interval.
     */
    public abstract void reportFunction();

    /**
     * This method kills the reporter thread.
     */
    public void killThread() {
        this.running = false;
    }

    /**
     * This method gets the environment object.
     *
     * @return the environment
     */
    public Environment getEnvironment() {
        return environment;
    }

    /**
     * This method gets the reporting interval.
     *
     * @return the interval
     */
    public long getReportingInterval() {
        return reportingInterval;
    }

    /**
     * This method sets the reporting interval.
     *
     * @param reportingInterval the new value
     */
    public void setReportingInterval(long reportingInterval) {
        this.reportingInterval = reportingInterval;
    }

    /**
     * This method gets the start time of the reporter thread.
     *
     * @return the start time
     */
    public long getStartTime() {
        return startTime;
    }
}
