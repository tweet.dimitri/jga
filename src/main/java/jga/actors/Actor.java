package jga.actors;

import jga.environments.Environment;
import jga.populations.Population;

/**
 * This interface describes an actor.
 */
public interface Actor {

    /**
     * This method gets called by the environment on the start of every generation.
     *
     * @param environment   the environment object itself
     * @param population    the population
     */
    void act(Environment environment, Population population);
}
