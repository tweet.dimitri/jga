package jga.utils.random;

import java.util.Random;

/**
 * This class holds the instance of the random.
 */
public final class UniformRandom extends Random {

    private static UniformRandom instance = new UniformRandom();

    /**
     * Constructor is private because class is final.
     */
    private UniformRandom() {
    }

    /**
     * Gets the instance of the uniform random.
     *
     * @return the instance
     */
    public static UniformRandom getInstance() {
        return instance;
    }
}
