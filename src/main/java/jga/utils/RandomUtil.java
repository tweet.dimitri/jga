package jga.utils;

import jga.random.RandomInterface;
import jga.random.UniformRandom;

/**
 * This final class holds the random object that is used all around the library.
 */
public final class RandomUtil {

    private static RandomInterface instance = new UniformRandom();

    /**
     * This method will replace the default random object by the new given object.
     * @param random the random object.
     */
    public static void useRandom(RandomInterface random) {
        RandomUtil.instance = random;
    }

    /**
     * This method will get the current random object instance.
     * @return the current instance
     */
    public static RandomInterface getInstance() {
        return instance;
    }
}
