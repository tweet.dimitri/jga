package jga.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class holds the instance of the executor pool.
 */
public final class ExecutorPool {

    private static int numThreads = 1;

    private static ExecutorService instance;

    /**
     * Constructor is private because class is final.
     */
    private ExecutorPool() {
    }

    /**
     * Gets the instance of the uniform random.
     *
     * @return the instance
     */
    public static ExecutorService getInstance() {
        if (instance == null) {
            instance = Executors.newFixedThreadPool(numThreads);
        }
        return instance;
    }

    /**
     * Gets the number of threads.
     *
     * @return the number of threads
     */
    public static int getNumThreads() {
        return numThreads;
    }

    /**
     * Sets the number of threads.
     *
     * @param numThreads the amount
     */
    public static void setNumThreads(int numThreads) {
        if (instance != null) {
            throw new IllegalStateException("You can't set the number of threads after using it!");
        }
        ExecutorPool.numThreads = numThreads;
    }
}