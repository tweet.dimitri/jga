package jga.datastructures;

/**
 * This interface describes a data structure object.
 *
 * @param <GeneType> the type of genes the data structure must handle
 */
public interface DataStructure<GeneType> {

    /**
     * This method gets a random new gene.
     * This can be based on the previous gene.
     * Needs to take into account previous being null.
     *
     * @param previous the Gene before this gene
     * @return the new gene
     */
    GeneType getRandom(GeneType previous);

    /**
     * This method gets a random new gene which is similar/close to the current one.
     * This can be based on the previous gene.
     * Needs to take into account previous being null.
     *
     * @param previous  the Gene before this gene
     * @param current   the current gene
     * @return the new gene
     */
    GeneType getCloseDelta(GeneType previous, GeneType current);

    /**
     * This method checks whether the current gene allows the next gene to exist.
     *
     * @param current   the current gene
     * @param next      the next gene
     * @return boolean whether the follow up is allowed
     */
    boolean isOption(GeneType current, GeneType next);

}
