package jga.datastructures;

import jga.utils.RandomUtil;

import java.util.List;

/**
 * This class implements the alphabet data structure.
 * Alphabets do not have constraints.
 *
 * @param <GeneType> the type of genes the data structure must handle
 */
public class Alphabet<GeneType> implements DataStructure<GeneType> {

    private List<GeneType> alphabet;

    /**
     * Constructor.
     *
     * @param alphabet the list of options in the alphabet
     */
    public Alphabet(List<GeneType> alphabet) {
        if (alphabet.isEmpty()) {
            throw new IllegalArgumentException("Alphabet can't be empty!");
        }
        this.alphabet = alphabet;
    }

    @Override
    public GeneType getRandom(GeneType previous) {
        int random = RandomUtil.getInstance().nextInt(alphabet.size());

        return alphabet.get(random);
    }

    @Override
    public GeneType getCloseDelta(GeneType previous, GeneType current) {
        int index = alphabet.indexOf(current);

        double random = RandomUtil.getInstance().nextGaussian();

        final int outerBoundary = 2;
        final int innerBoundary = 1;

        final int maxShift = 3;
        final int mediumShift = 2;
        final int minShift = 1;

        if (random > outerBoundary) {
            index += maxShift;
        } else if (random < -outerBoundary)  {
            index -= maxShift;
        } else if (random > innerBoundary) {
            index += mediumShift;
        } else  if (random < -innerBoundary) {
            index -= mediumShift;
        } else if (random > 0) {
            index += minShift;
        } else {
            index -= minShift;
        }

        while (index < 0) {
            index = alphabet.size() + index;
        }

        while (index >= alphabet.size()) {
            index = index - alphabet.size();
        }

        return alphabet.get(index);
    }

    @Override
    public boolean isOption(GeneType current, GeneType next) {
        return true;
    }
}
