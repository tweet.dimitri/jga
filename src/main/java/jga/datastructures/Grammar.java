package jga.datastructures;

import jga.utils.RandomUtil;

import java.util.*;

/**
 * This class implements the grammar data structure.
 * Grammars do have constraints.
 *
 * @param <GeneType> the type of genes the data structure must handle
 */
public class Grammar<GeneType> implements DataStructure<GeneType> {

    // TODO add like a value option where values can be assigned to branches
    //  to give them higher probability

    private List<GeneType> roots;
    private Map<GeneType, List<GeneType>> graph;

    /**
     * Constructor.
     *
     * @param roots the list of root options
     */
    public Grammar(List<GeneType> roots) {
        this.graph = new HashMap<>();
        this.roots = roots;
        for (GeneType root : roots) {
            graph.put(root, new ArrayList<>());
        }
    }

    /**
     * This method adds an option to a certain root.
     *
     * @param root      the root to add an option to
     * @param branch    the option to add
     */
    public void addBranch(GeneType root, GeneType branch) {
        graph.putIfAbsent(root, new ArrayList<>());

        graph.get(root).add(branch);

        graph.putIfAbsent(branch, new ArrayList<>());
    }

    /**
     * This method adds a list of options to a certain root.
     *
     * @param root      the root to add the options to
     * @param branches  the options to add
     */
    public void addBranches(GeneType root, List<GeneType> branches) {
        graph.putIfAbsent(root, new ArrayList<>());

        graph.get(root).addAll(branches);

        for (GeneType branch : branches) {
            graph.putIfAbsent(branch, new ArrayList<>());
        }
    }

    /**
     * This method adds a list of options to a list of roots.
     *
     * @param roots     the roots to add the options to
     * @param branches  the options to add
     */
    public void addBranches(List<GeneType> roots, List<GeneType> branches) {
        for (GeneType root : roots) {
            addBranches(root, branches);
        }
    }

    /**
     * This method adds an option to a list of roots.
     *
     * @param roots     the roots to add the option to
     * @param branch    the option to add
     */
    public void addBranches(List<GeneType> roots, GeneType branch) {
        for (GeneType root : roots) {
            addBranch(root, branch);
        }
    }

    /**
     * This method gets all the options for a certain root.
     *
     * @param node the root to get the options for
     * @return the options of the root
     */
    public List<GeneType> getOptions(GeneType node) {
        return graph.get(node);
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();

        Iterator<GeneType> iterator = graph.keySet().iterator();
        while (iterator.hasNext()) {
            GeneType key = iterator.next();
            List<GeneType> values = graph.get(key);
            out.append("\"").append(key).append("\"").append(" -> {\n");
            for (GeneType value : values) {
                out.append("\t\"").append(value).append("\"\n");
            }
            out.append("}\n");
        }

        return out.toString();
    }

    @Override
    public GeneType getRandom(GeneType previous) {
        if (previous == null) {
            int random = RandomUtil.getInstance().nextInt(roots.size());

            return roots.get(random);
        }

        List<GeneType> options = getOptions(previous);

        if (options.isEmpty()) {
            return null;
        }


        int random = RandomUtil.getInstance().nextInt(options.size());

        return options.get(random);
    }

    @Override
    public GeneType getCloseDelta(GeneType previous, GeneType current) {
        return getRandom(previous);
    }

    @Override
    public boolean isOption(GeneType current, GeneType next) {
        return getOptions(current).contains(next);
    }
}