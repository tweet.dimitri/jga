package jga.datastructures;

import jga.utils.RandomUtil;

/**
 * This class implements the double rnage data structure.
 * Its constraints are the min, max and step size.
 */
public class DoubleRange implements DataStructure<Double> {

    private double min;
    private double max;
    private double stepSize;

    private double middle;

    private boolean useRange;

    /**
     * Constructor.
     * 
     * @param min       the minimum value
     * @param max       the maximum value
     * @param stepSize  the step size
     */
    public DoubleRange(double min, double max, double stepSize) {
        this.min = min;
        this.max = max;
        this.stepSize = stepSize;
        this.useRange = true;
    }

    /**
     * Constructor.
     *
     * @param middle    the middle value
     * @param stepSize  the step size
     */
    public DoubleRange(double middle, double stepSize) {
        this.middle = middle;
        this.stepSize = stepSize;
        this.useRange = false;
    }

    @Override
    public Double getRandom(Double previous) {
        if (useRange) {
            return (RandomUtil.getInstance().nextDouble() * (max - min)) + min;
        }

        final double normalization = 0.5d;
        return (RandomUtil.getInstance().nextDouble() - normalization) * stepSize + middle;
    }

    @Override
    public Double getCloseDelta(Double previous, Double current) {
        final int maxShift = 5;
        return current + ((RandomUtil.getInstance().nextInt(maxShift) - 2d) * stepSize);
    }

    @Override
    public boolean isOption(Double current, Double next) {
        return true;
    }
}
