package jga.individuals;

import jga.datastructures.DataStructure;
import jga.problems.Problem;
import jga.utils.ExecutorPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * This class is an abstract version of an individual.
 *
 * @param <DNA> the dna structure
 * @param <GeneType> what the dna is composed of
 * @param <DataObject> the type of data the individual will receive
 */
public abstract class Individual<DNA, GeneType, DataObject> {

    private Problem<DNA, DataObject> problem;
    private DataStructure<GeneType> dataStructure;
    private DNA dna;
    private int dnaLength;

    private double[] fitness;
    private Future<double[]> future;

    private boolean reprocess;

    private int score;
    private int age;

    private boolean variableChromosomeLength;

    private int[] hierarchyPath;

    private List<Individual> parents;

    private boolean dnaChanged;

    /**
     * Constructor.
     *
     * @param problem                   the problem object
     * @param dataStructure             the data structure object
     * @param chromosomeSizes           the array of sizes of the individual's chromosomes
     * @param variableChromosomeLength  whether the chromosomes are of variable length
     */
    public Individual(Problem<DNA, DataObject> problem,
                      DataStructure<GeneType> dataStructure,
                      int[] chromosomeSizes,
                      boolean variableChromosomeLength) {
        this.problem = problem;
        this.dataStructure = dataStructure;
        this.dna = generateDNA(chromosomeSizes);
        this.score = 0;
        this.age = 0;
        this.variableChromosomeLength = variableChromosomeLength;
        this.hierarchyPath = new int[0];
        this.reprocess = false;
        this.parents = new ArrayList<>(2);
        this.dnaChanged = true;


        setDnaLength(calculateSize());
    }

    /**
     * Constructor.
     *
     * @param parent    the parent of the individual
     * @param dna       the dna of the new individual
     */
    public Individual(Individual<DNA, GeneType, DataObject> parent,
                      DNA dna) {
        this.problem = parent.getProblem();
        this.dataStructure = parent.getDataStructure();
        this.dna = dna;
        this.score = 0;
        this.age = 0;
        this.variableChromosomeLength = parent.hasVariableChromosomeLength();
        this.hierarchyPath = parent.getHierarchyPath().clone();
        this.reprocess = parent.isReprocess();
        this.parents = new ArrayList<>(2);

        this.parents.add(parent);
        this.dnaChanged = false;

        setDnaLength(calculateSize());
    }

    /**
     * Constructor.
     *
     * @param parent1   the first parent
     * @param parent2   the second parent
     * @param dna       the dna of the new individual
     */
    public Individual(Individual<DNA, GeneType, DataObject> parent1,
                      Individual<DNA, GeneType, DataObject> parent2,
                      DNA dna) {
        this.problem = parent1.getProblem();
        this.dataStructure = parent1.getDataStructure();
        this.dna = dna;
        this.score = 0;
        this.age = 0;
        this.variableChromosomeLength = parent1.hasVariableChromosomeLength();
        this.hierarchyPath = parent1.getHierarchyPath().clone();
        this.reprocess = parent1.isReprocess();
        this.parents = new ArrayList<>(2);

        this.parents.add(parent1);
        this.parents.add(parent2);

        this.dnaChanged = true;

        setDnaLength(calculateSize());
    }

    /**
     * This method initializes the dna.
     *
     * @param chromosomeSizes the maximum sizes of the chromosomes
     * @return the dna
     */
    public abstract DNA generateDNA(int[] chromosomeSizes);

    /**
     * This method does the crossover operation for the chromosome type the individual has.
     *
     * @param crossoverPoint the point where the crossover should start
     * @param other          the other individual to do crossover with
     * @return the newly created individual
     */
    public abstract Individual crossover(int crossoverPoint, Individual<DNA, GeneType, DataObject> other);


    /**
     * This method does the add mutation operation
     * for the chromosome type the individual has.
     *
     * @param position the position that has to mutated
     */
    public abstract void addMutation(int position);

    /**
     * This method does the remove mutation operation
     * for the chromosome type the individual has.
     *
     * @param position the position that has to mutated
     */
    public abstract void removeMutation(int position);

    /**
     * This method does the point mutation operation
     * for the chromosome type the individual has.
     *
     * @param position the position that has to mutated
     */
    public abstract void pointMutation(int position);

    /**
     * This method does the point mutation operation
     * for the chromosome type the individual has.
     *
     * @param position the position that has to mutated
     */
    public abstract void deltaMutation(int position);

    /**
     * This method calculates the current size of the dna.
     *
     * @return the current size
     */
    public abstract int calculateSize();

    /**
     * This method makes a deep copy of the individual.
     *
     * @return a deep copy of the individual
     */
    public abstract Individual<DNA, GeneType, DataObject> copy();

    /**
     * This method feeds the new data to the individual using the problem object.
     * It creates a future of the fitness array.
     *
     * @param dataObject the data object to process
     */
    public void processData(DataObject dataObject) {
        reset();

        if (future != null && fitness != null && !reprocess) {
            return;
        }

        if (!dnaChanged && !parents.isEmpty() && parents.get(0).fitness != null) {
            fitness = parents.get(0).fitness.clone();
            return;
        }

        Callable<double[]> callable = () -> problem.process(this, getDNA(), dataObject);

        future = ExecutorPool.getInstance().submit(callable);
    }

    /**
     * This method returns a clone of the fitness array of the individual.
     * It checks whether there is a future available with a new fitness array.
     *
     * @return the cloned fitness array
     */
    public double[] getFitness() {
        if (future != null) {
            try {
                fitness = future.get();
                future = null;

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        if (fitness == null) {
            return null;
        }

        return fitness.clone();
    }

    /**
     * This method resets the score of the individual.
     * and updates the age property.
     */
    public void reset() {
        this.score = 0;
        this.age++;
    }

    /**
     * This method gets the DNA length.
     * It throws an exception when the dna length is zero or less.
     *
     * @return the length
     */
    public int getDNALength() {
        if (dnaLength == 0) {
            throw new IllegalStateException("DNA length is zero!");
        } else if (dnaLength < 0) {
            throw new IllegalStateException("DNA length is below zero!");
        }

        return dnaLength;
    }

    /**
     * This method sets the DNA length.
     *
     * @param dnaLength the new value
     */
    public void setDnaLength(int dnaLength) {
        this.dnaLength = dnaLength;
    }

    /**
     * This method gets the score of the individual.
     *
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * This method sets the score of the individual.
     *
     * @param score the new score
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * This method gets the amount of survived generations.
     *
     * @return the amount of survived generations.
     */
    public int getAge() {
        return age;
    }

    /**
     * This method gets the DNA of the individual.
     *
     * @return the DNA
     */
    public DNA getDNA() {
        return dna;
    }

    /**
     * This method sets the DNA of the individual.
     *
     * @param dna the new DNA
     */
    public void setDNA(DNA dna) {
        this.dna = dna;
        setDnaChanged(true);
    }

    /**
     * This method gets the problem object of the individual.
     *
     * @return the problem object
     */
    public Problem<DNA, DataObject> getProblem() {
        return problem;
    }

    /**
     * This method gets the data structure that the individual uses for its dna.
     *
     * @return the used data structure
     */
    public DataStructure<GeneType> getDataStructure() {
        return dataStructure;
    }

    /**
     * This method checks whether the chromosomes of the individual are of variable length.
     *
     * @return boolean whether this individual has variable chromosome length.
     */
    public boolean hasVariableChromosomeLength() {
        return variableChromosomeLength;
    }

    /**
     * This method sets the new hierarchy path of the individual.
     * This method should be used by for example a shuffle/migration actor.
     *
     * @param hierarchyPath the new hierarchy path
     */
    public void setHierarchyPath(int[] hierarchyPath) {
        this.hierarchyPath = hierarchyPath.clone();
    }

    /**
     * This method gets the hierarchy path of this individual.
     * This can be used to deduce in which sub population this individual resides.
     * For example, { 3, 2 } means that the individual resides in the 3th sub population,
     * of which it is in the 2nd sub sub population.
     *
     * @return the hierarchy path
     */
    public int[] getHierarchyPath() {
        return hierarchyPath.clone();
    }

    /**
     * This method checks whether this individual should reprocess to get a new fitness array.
     * This could be needed when for example,
     * the data changes or the individual must be tested on several datasets.
     *
     * @return boolean whether the individual should reprocess
     */
    public boolean isReprocess() {
        return reprocess;
    }

    /**
     * Sets whether the individuals should reprocess.
     *
     * @param reprocess the new boolean value
     */
    public void setReprocess(boolean reprocess) {
        this.reprocess = reprocess;
    }

    /**
     * This method gets the direct parents of the individual.
     *
     * @return the list of direct parents.
     */
    public List<Individual> getParents() {
        return parents;
    }

    public boolean isDnaChanged() {
        return dnaChanged;
    }

    public void setDnaChanged(boolean dnaChanged) {
        this.dnaChanged = dnaChanged;
    }
}
