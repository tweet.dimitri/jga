package jga.individuals;

import jga.datastructures.DataStructure;
import jga.problems.Problem;
import jga.utils.RandomUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the Grammar Individual.
 *
 * @param <GeneType> what the dna is composed of
 * @param <DataObject> the type of data the individual will receive
 */
public class GrammarIndividual<GeneType, DataObject> extends Individual<List<List<GeneType>>, GeneType, DataObject> {

    /**
     * Constructor.
     *
     * @param problem                   the problem object
     * @param dataStructure             the data structure object
     * @param chromosomeSizes           the array of sizes of the individual's chromosomes
     * @param variableChromosomeLength  whether the chromosomes are of variable length
     */
    public GrammarIndividual(Problem<List<List<GeneType>>, DataObject> problem,
                             DataStructure<GeneType> dataStructure,
                             int[] chromosomeSizes,
                             boolean variableChromosomeLength) {
        super(problem, dataStructure, chromosomeSizes, variableChromosomeLength);
    }

    /**
     * Constructor.
     *
     * @param parent    the parent of the individual
     * @param dna       the dna of the new individual
     */
    public GrammarIndividual(Individual<List<List<GeneType>>, GeneType, DataObject> parent,
                             List<List<GeneType>> dna) {
        super(parent, dna);
    }

    /**
     * Constructor.
     *
     * @param parent1   the first parent
     * @param parent2   the second parent
     * @param dna       the dna of the new individual
     */
    public GrammarIndividual(Individual<List<List<GeneType>>, GeneType, DataObject> parent1,
                             Individual<List<List<GeneType>>, GeneType, DataObject> parent2,
                             List<List<GeneType>> dna) {
        super(parent1, parent2, dna);
    }

    @Override
    public List<List<GeneType>> generateDNA(int[] chromosomeSizes) {
        if (chromosomeSizes == null || chromosomeSizes.length == 0) {
            throw new IllegalArgumentException("There has to be at least one chromosome!");
        }

        List<List<GeneType>> dna = new ArrayList<>(chromosomeSizes.length);

        for (int chromosomeSize : chromosomeSizes) {
            if (chromosomeSize < 1) {
                throw new IllegalArgumentException("Chromosome size can't be smaller than 1!");
            }

            int chromosomeLength = chromosomeSize;

            if (hasVariableChromosomeLength()) {
                chromosomeLength = RandomUtil.getInstance().nextInt(chromosomeSize) + 1;
            }

            List<GeneType> chromosome = new ArrayList<>(chromosomeLength);

            for (int j = 0; j < chromosomeLength; j++) {
                GeneType previous = null;

                if (j != 0) {
                    previous = chromosome.get(j - 1);
                }

                GeneType gene = getDataStructure().getRandom(previous);

                if (gene == null) {
                    break;
                }

                chromosome.add(gene);
            }
            dna.add(chromosome);
        }
        return dna;
    }

    @Override
    public Individual crossover(int crossoverPoint,
                                Individual<List<List<GeneType>>, GeneType, DataObject> other) {
        List<List<GeneType>> offspringDNA = new ArrayList<>();

        int count = 0;
        for (int i = 0; i < getDNA().size(); i++) {
            List<GeneType> chromosome = new ArrayList<>();
            for (int j = 0; j < getDNA().get(i).size(); j++) {
                if (count < crossoverPoint) {
                    chromosome.add(getDNA().get(i).get(j));
                } else {
                    if (j < other.getDNA().get(i).size()) {
                        chromosome.add(other.getDNA().get(i).get(j));
                    } else {
                        break;
                    }
                }
                count++;
            }

            offspringDNA.add(chromosome);
        }

        return new GrammarIndividual<>(this, other, offspringDNA);
    }

    @Override
    public void addMutation(int position) {
        int pos = position;

        setDnaChanged(true);

        for (int i = 0; i < getDNA().size(); i++) {
            if (pos - (getDNA().get(i).size() + 1) >= 0 && i + 1 != getDNA().size()) {
                pos -= getDNA().get(i).size();
            } else {
                GeneType previous = getPrevious(i, pos);

                GeneType newGene = getDataStructure().getRandom(previous);

                getDNA().get(i).add(pos, newGene);
                fixBrokenDna(i, pos);

                if (!hasVariableChromosomeLength()) {
                    getDNA().get(i).remove(getDNA().get(i).size() - 1);
                }

                setDnaLength(calculateSize());
                return;
            }
        }


    }

    @Override
    public void removeMutation(int position) {
        int pos = position;

        setDnaChanged(true);

        for (int i = 0; i < getDNA().size(); i++) {
            if (pos - getDNA().get(i).size() >= 0) {
                pos -= getDNA().get(i).size();
            } else {
                if (getDNA().get(i).size() < 2) {
                    addMutation(position);
                    return;
                }

                getDNA().get(i).remove(pos);

                if (pos != getDNA().get(i).size()) {
                    fixBrokenDna(i, pos);
                }

                if (!hasVariableChromosomeLength()) {
                    int index = getDNA().size() - 1;
                    GeneType last = getDNA().get(index).get(getDNA().get(index).size() - 1);
                    GeneType newLast = getDataStructure().getRandom(last);

                    if (newLast != null) {
                        getDNA().get(i).add(newLast);
                    }
                }

                setDnaLength(calculateSize());
                break;
            }
        }
    }

    @Override
    public void pointMutation(int position) {
        int pos = position;

        setDnaChanged(true);

        for (int i = 0; i < getDNA().size(); i++) {
            if (pos - getDNA().get(i).size() >= 0) {
                pos -= getDNA().get(i).size();
            } else {
                GeneType previous = getPrevious(i, pos);

                GeneType newGene = getDataStructure().getRandom(previous);

                getDNA().get(i).set(pos, newGene);
                fixBrokenDna(i, pos);
                setDnaLength(calculateSize());
                return;
            }
        }
    }

    @Override
    public void deltaMutation(int position) {
        int pos = position;

        setDnaChanged(true);

        for (int i = 0; i < getDNA().size(); i++) {
            if (pos - getDNA().get(i).size() >= 0) {
                pos -= getDNA().get(i).size();
            } else {
                GeneType previous = getPrevious(i, pos);

                GeneType current = getDNA().get(i).get(pos);

                GeneType newGene = getDataStructure().getCloseDelta(previous, current);
                getDNA().get(i).set(pos, newGene);

                fixBrokenDna(i, pos);
                setDnaLength(calculateSize());
                return;
            }
        }
    }

    @Override
    public int calculateSize() {
        int size = 0;
        for (int i = 0; i < getDNA().size(); i++) {
            size += getDNA().get(i).size();
        }
        return size;
    }

    /**
     * Gets the previous gene.
     *
     * @param listIndex the index of the list the gene is in
     * @param index the index of the gene in the list
     * @return the previous gene
     */
    private GeneType getPrevious(int listIndex, int index) {
        if (index == 0) {
            if (listIndex != 0) {
                int size = getDNA().get(listIndex - 1).size();
                return getDNA().get(listIndex - 1).get(size - 1);
            }
        } else {
            return getDNA().get(listIndex).get(index - 1);
        }
        return null;
    }

    /**
     * Repair the dna that has been broken for some reason.
     * As this is a grammar individual dna is considered broken,
     * if for example, gene A cannot be after gene B, but yet it is in the dna.
     *
     * @param listIndex the index of the list that has changed
     * @param index the index of the gene in the list that has changed
     */
    private void fixBrokenDna(int listIndex, int index) {
        if (index == getDNA().get(listIndex).size() - 1) {
            // The rest can stay the same
            return;
        }

        GeneType newGene = getDNA().get(listIndex).get(index);

        if (getDataStructure().isOption(newGene, getDNA().get(listIndex).get(index + 1))) {
            // The rest can stay the same
            return;
        }

        List<GeneType> oldList = getDNA().get(listIndex);

        List<GeneType> newList = new ArrayList<>();

        for (int i = 0; i < index; i++) {
            newList.add(oldList.get(i));
        }

        GeneType last = null;
        while (newList.size() != oldList.size()) {
            last = getDataStructure().getRandom(last);
            if (last == null) {
                break;
            } else {
                newList.add(last);
            }
        }

    }

    @Override
    public Individual<List<List<GeneType>>, GeneType, DataObject> copy() {
        List<List<GeneType>> copyDNA = new ArrayList<>();

        for (int i = 0; i < getDNA().size(); i++) {
            List<GeneType> copyChromosome = new ArrayList<>(getDNA().get(i));
            copyDNA.add(copyChromosome);
        }

        return new GrammarIndividual<>(this, copyDNA);
    }
}
