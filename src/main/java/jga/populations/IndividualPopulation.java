package jga.populations;

import jga.individuals.Individual;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a population consisting of individuals.
 */
public class IndividualPopulation extends Population<Individual> {

    @Override
    public IndividualPopulation toIndividualPopulation() {
        return this;
    }

    @Override
    public boolean containsIndividual(Individual individual) {
        return contains(individual);
    }

    @Override
    public IndividualPopulation deepCopy() {
        IndividualPopulation individuals = new IndividualPopulation();

        individuals.addAll(this);

        return individuals;
    }

    @Override
    public List<Integer> buildSizeArray() {
        List<Integer> sizeArray = new ArrayList<>();

        sizeArray.add(size());

        return sizeArray;
    }

    @Override
    public void setHierarchyPaths() {
        for (int i = 0; i < size(); i++) {
            get(i).setHierarchyPath(new int[0]);
        }
    }
}
