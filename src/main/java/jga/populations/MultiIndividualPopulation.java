package jga.populations;

import jga.individuals.Individual;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a population consisting of individual populations.
 */
public class MultiIndividualPopulation extends MultiPopulation<IndividualPopulation> {

    @Override
    public boolean containsIndividual(Individual individual) {
        for (int i = 0; i < size(); i++) {
            if (get(i).contains(individual)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public MultiIndividualPopulation deepCopy() {
        MultiIndividualPopulation multiIndividualPopulation = new MultiIndividualPopulation();

        for (int i = 0; i < size(); i++) {
            IndividualPopulation individuals = new IndividualPopulation();
            individuals.addAll(get(i));
            multiIndividualPopulation.add(individuals);
        }

        return multiIndividualPopulation;
    }

    @Override
    public List<Integer> buildSizeArray() {
        List<Integer> sizeArray = new ArrayList<>();

        sizeArray.add(size());

        for (int i = 0; i < size(); i++) {
            sizeArray.add(get(i).size());
        }

        return sizeArray;
    }

    @Override
    public void setHierarchyPaths() {
        for (int i = 0; i < size(); i++) {
            for (int j = 0; j < get(i).size(); j++) {
                get(i).get(j).setHierarchyPath(new int[]{i});
            }
        }
    }
}
