package jga.populations;

/**
 * This class implements a population consisting of sub populations.
 *
 * @param <T> the population type the sub populations will consist of
 */
public abstract class MultiPopulation<T extends Population> extends Population<T> {

    private int migrations = 0;

    @Override
    public IndividualPopulation toIndividualPopulation() {
        IndividualPopulation individuals = new IndividualPopulation();
        for (int i = 0; i < size(); i++) {
            individuals.addAll(get(i).toIndividualPopulation());
        }
        return individuals;
    }

    /**
     * Get the amount of migrations.
     * @return the amount
     */
    public int getMigrations() {
        return migrations;
    }

    /**
     * Set the amount of migrations.
     * @param migrations the new amount
     */
    public void setMigrations(int migrations) {
        this.migrations = migrations;
    }
}
