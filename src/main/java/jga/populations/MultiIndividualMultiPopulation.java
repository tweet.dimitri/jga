package jga.populations;

import jga.individuals.Individual;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a population consisting of islands consisting of family populations.
 */
public class MultiIndividualMultiPopulation extends MultiPopulation<MultiIndividualPopulation> {

    @Override
    public boolean containsIndividual(Individual individual) {
        for (MultiIndividualPopulation populations : this) {
            if (populations.containsIndividual(individual)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public MultiIndividualMultiPopulation deepCopy() {
        MultiIndividualMultiPopulation multiIndividualMultiPopulation = new MultiIndividualMultiPopulation();

        for (int i = 0; i < size(); i++) {
            MultiIndividualPopulation familyPopulation = new MultiIndividualPopulation();
            for (int j = 0; j < get(i).size(); j++) {
                IndividualPopulation individuals = new IndividualPopulation();
                individuals.addAll(get(i).get(j));
                familyPopulation.add(individuals);
            }
            multiIndividualMultiPopulation.add(familyPopulation);
        }

        return multiIndividualMultiPopulation;
    }

    @Override
    public List<Integer> buildSizeArray() {
        List<Integer> sizeArray = new ArrayList<>();

        sizeArray.add(size());

        for (int i = 0; i < size(); i++) {
            sizeArray.add(get(i).size());
            for (int j = 0; j < get(i).size(); j++) {
                sizeArray.add(get(i).get(j).size());
            }
        }

        return sizeArray;
    }

    @Override
    public void setHierarchyPaths() {
        for (int i = 0; i < size(); i++) {
            for (int j = 0; j < get(i).size(); j++) {
                for (int k = 0; k < get(i).get(j).size(); k++) {
                    get(i).get(j).get(k).setHierarchyPath(new int[]{i, j});
                }
            }
        }
    }
}
