package jga.populations;

import jga.individuals.Individual;

import java.util.*;

/**
 * This class describes a population.
 * Which can consist of any object.
 *
 * @param <T> the object type the population will consist of
 */
public abstract class Population<T> extends ArrayList<T> {

    private Individual bestIndividual;

    /**
     * This method is used to turn any population in an individual population.
     *
     * @return an individual population
     */
    public abstract IndividualPopulation toIndividualPopulation();

    /**
     * This method checks whether a population contains a certain individual.
     *
     * @param individual the individual to test
     * @return the boolean value
     */
    public abstract boolean containsIndividual(Individual individual);

    /**
     * This method creates a deep copy of the entire population.
     *
     * @return the deep copy of the population
     */
    public abstract Population<T> deepCopy();

    /**
     * This method creates a list of integers.
     * This list represents the sizes of the population and its sub populations.
     *
     * @return an array of the sizes of the population and its sub populations
     */
    public abstract List<Integer> buildSizeArray();

    /**
     * This method sets the hierarchy paths of the individuals.
     * This method gets called after for example the migration operation.
     * Example: individual A resides in sub population with index 6,
     * then the hierarchy path must be set to { 6 }
     */
    public abstract void setHierarchyPaths();

    /**
     * This method gets the best scoring individual of the population.
     *
     * @return the best scoring individual
     */
    public Individual getBestIndividual() {
        return bestIndividual;
    }

    /**
     * This method sets the best scoring individual of the population.
     *
     * @param bestIndividual the new individual
     */
    public void setBestIndividual(Individual bestIndividual) {
        this.bestIndividual = bestIndividual;
    }
}
