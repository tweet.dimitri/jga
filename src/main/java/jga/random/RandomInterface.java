package jga.random;

public interface RandomInterface {

    double nextDouble();

    int nextInt();

    int nextInt(int bound);

    double nextGaussian();

    boolean nextBoolean();
}
