package jga.random;

import java.util.Random;

/**
 * This class holds the instance of the uniform random.
 */
public class UniformRandom extends Random implements RandomInterface {}
