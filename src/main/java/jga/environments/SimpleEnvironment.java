package jga.environments;

import jga.individuals.Individual;
import jga.operators.crossover.Crossover;
import jga.operators.mutation.Mutation;
import jga.operators.scoring.Scoring;
import jga.operators.selection.Selection;
import jga.populations.IndividualPopulation;

/**
 * This class implements a simple environment.
 *
 * @param <DataObject> the type of data object
 */
public class SimpleEnvironment<DataObject>
        extends Environment<DataObject, IndividualPopulation> {

    /**
     * Constructor of the environment.
     *
     * @param scoring   the comparator object
     * @param selection the selection object
     * @param crossover the crossover object
     * @param mutation  the simpleMutation object
     */
    public SimpleEnvironment(Scoring scoring,
                             Selection selection,
                             Crossover crossover,
                             Mutation mutation) {
        super(scoring, selection, crossover, mutation);
    }

    @Override
    public IndividualPopulation execute(DataObject dataObject,
                                        IndividualPopulation individuals) {
        IndividualPopulation population = individuals.toIndividualPopulation();

        setStage("Breed");
        IndividualPopulation offspring = getCrossover().breed(population, population.size() * 2);

        setStage("Mutate");
        getMutation().mutate(offspring);

        setStage("Process");
        for (Individual individual : offspring) {
            individual.processData(dataObject);
        }

        population.addAll(offspring);

        setStage("Score");
        getScoring().setScoresAndSort(population);

        setStage("Select");

        return getSelection().select(population, getSizeArray().get(0));
    }
}
