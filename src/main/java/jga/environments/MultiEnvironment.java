package jga.environments;

import jga.individuals.Individual;
import jga.operators.crossover.Crossover;
import jga.operators.mutation.Mutation;
import jga.operators.scoring.Scoring;
import jga.operators.selection.Selection;
import jga.populations.IndividualPopulation;
import jga.populations.MultiIndividualPopulation;
import jga.populations.MultiPopulation;

import java.util.HashSet;
import java.util.Set;

/**
 * This class implements a multi population environment.
 *
 * @param <DataObject> the type of data object
 */
public class MultiEnvironment<DataObject>
        extends Environment<DataObject, MultiPopulation<IndividualPopulation>> {

    /**
     * Constructor of the environment.
     *
     * @param scoring   the comparator object
     * @param selection the selection object
     * @param crossover the crossover object
     * @param mutation  the simpleMutation object
     */
    public MultiEnvironment(Scoring scoring,
                            Selection selection,
                            Crossover crossover,
                            Mutation mutation) {
        super(scoring, selection, crossover, mutation);
    }

    @Override
    public MultiPopulation<IndividualPopulation> execute(DataObject dataObject,
                                                         MultiPopulation<IndividualPopulation> population) {
        Set<Integer> currentSkipList = new HashSet<>(getSkipList());

        for (int i = 0; i < population.size(); i++) {
            if (currentSkipList.contains(i)) {
                continue;
            }

            IndividualPopulation island = population.get(i);
            setStage("Breed");
            IndividualPopulation children = getCrossover().breed(island, island.size() * 2);
            island.addAll(children);
            setStage("Mutate");
            getMutation().mutate(children);
            setStage("Process");
            for (Individual individual : island) {
                individual.processData(dataObject);
            }
        }

        MultiIndividualPopulation survivors = new MultiIndividualPopulation();

        for (int i = 0; i < population.size(); i++) {
            IndividualPopulation island = population.get(i);
            if (currentSkipList.contains(i)) {
                survivors.add(island);
                continue;
            }
            setStage("Score");
            getScoring().setScoresAndSort(island);
            setStage("Select");
            IndividualPopulation selected = getSelection().select(island, getSizeArray().get(i + 1));
            survivors.add(selected);
        }
        return survivors;
    }
}
