package jga.environments;

import jga.individuals.Individual;
import jga.operators.crossover.Crossover;
import jga.operators.mutation.Mutation;
import jga.operators.scoring.Scoring;
import jga.operators.selection.Selection;
import jga.populations.*;

import java.util.HashSet;
import java.util.Set;

/**
 * This class implements a multi multi population.
 *
 * @param <DataObject> the type of data object
 */
public class MultiMultiEnvironment<DataObject>
        extends Environment<DataObject, MultiPopulation<MultiIndividualPopulation>> {

    /**
     * Constructor of the environment.
     *
     * @param scoring   the scoring object
     * @param selection the selection object
     * @param crossover the crossover object
     * @param mutation  the simpleMutation object
     */
    public MultiMultiEnvironment(Scoring scoring,
                                 Selection selection,
                                 Crossover crossover,
                                 Mutation mutation) {
        super(scoring, selection, crossover, mutation);
    }

    @Override
    public MultiPopulation<MultiIndividualPopulation> execute(DataObject dataObject,
                                                              MultiPopulation<MultiIndividualPopulation> population) {
        Set<Integer> currentSkipList = new HashSet<>(getSkipList());
        // TODO this is not the right way as it is not the corresponding family size
        // TODO to fix this the sizearray needs to be structured differently
        final int familySize = getSizeArray().get(2);
        MultiIndividualMultiPopulation survivors = new MultiIndividualMultiPopulation();

        for (int i = 0; i < population.size(); i++) {
            MultiIndividualPopulation island = population.get(i);
            if (currentSkipList.contains(i)) {
                survivors.add(island);
                continue;
            }
            for (IndividualPopulation family : island) {
                setStage("Breed");
                IndividualPopulation children = getCrossover().breed(family, family.size() * 2);
                family.addAll(children);
                setStage("Mutate");
                getMutation().mutate(children);
                setStage("Process");
                for (Individual individual : family) {
                    individual.processData(dataObject);
                }
            }

            MultiIndividualPopulation islandSurvivors = new MultiIndividualPopulation();
            for (IndividualPopulation family : island) {
                setStage("Score");
                getScoring().setScoresAndSort(family);
                setStage("Select");
                IndividualPopulation selected = getSelection().select(family, familySize);
                islandSurvivors.add(selected);
            }

            survivors.add(islandSurvivors);
        }

        return survivors;
    }
}
