package jga.environments;

import jga.actors.Actor;
import jga.individuals.Individual;
import jga.operators.crossover.Crossover;
import jga.operators.mutation.Mutation;
import jga.operators.scoring.Scoring;
import jga.operators.selection.Selection;
import jga.populations.IndividualPopulation;
import jga.populations.Population;

import java.util.*;

/**
 * This class describes the abstract form of an environment.
 *
 * @param <DataObject> the type of data object
 * @param <PopulationType> the type of population
 */
public abstract class Environment<DataObject, PopulationType extends Population> {

    private Scoring scoring;
    private Selection selection;
    private Crossover crossover;
    private Mutation mutation;

    private List<Actor> actorList;

    private int gen = 0;
    private String stage = "";

    private List<Integer> sizeArray;

    private boolean solutionFoundFlag;
    private Set<Integer> skipList;

    /**
     * Constructor.
     *
     * @param scoring   the scoring operator
     * @param selection the selection operator
     * @param crossover the crossover operator
     * @param mutation  the mutation operator
     */
    public Environment(Scoring scoring,
                       Selection selection,
                       Crossover crossover,
                       Mutation mutation) {
        this.scoring = scoring;
        this.selection = selection;
        this.crossover = crossover;
        this.mutation = mutation;
        this.skipList = new HashSet<>();
        this.actorList = new ArrayList<>();
        this.solutionFoundFlag = false;
    }

    /**
     * This method executes the specified amount of generations to optimize for the given data.
     * It also stops when the solution found flag is true.
     *
     * @param dataObject    the data
     * @param population    the current population
     * @param generations   the amount of generations to do
     * @return              the resulting population
     */
    public PopulationType executeGenerations(DataObject dataObject,
                                             PopulationType population,
                                             int generations) {

        PopulationType currentPopulation = (PopulationType) population.deepCopy();

        sizeArray = population.buildSizeArray();

        population.setHierarchyPaths();
        feedInitialPopulation(dataObject, currentPopulation);
        this.gen = 0;

        for (int gen = 0; gen < generations; gen++) {
            if (solutionFoundFlag) {
                break;
            }

            currentPopulation = executeOne(dataObject, currentPopulation);
        }

        return currentPopulation;
    }

    /**
     * This method executes until the specified amount of time has passed.
     * It also stops when the solution found flag is true.
     *
     * @param dataObject        the data
     * @param population        the current population
     * @param maxTimeMinutes    the amount of minutes to run
     * @return                  the resulting population
     */
    public PopulationType executeTimeLimit(DataObject dataObject,
                                           PopulationType population,
                                           double maxTimeMinutes) {

        PopulationType currentPopulation = (PopulationType) population.clone();

        sizeArray = population.buildSizeArray();

        population.setHierarchyPaths();
        feedInitialPopulation(dataObject, currentPopulation);
        this.gen = 0;

        final double millisPerMinute = 60d * 1000d;

        long maxTime = (long) (maxTimeMinutes * millisPerMinute);

        long last = System.currentTimeMillis();

        while (System.currentTimeMillis() - last < maxTime) {
            if (solutionFoundFlag) {
                break;
            }

            currentPopulation = executeOne(dataObject, currentPopulation);
        }

        return currentPopulation;
    }

    /**
     * This method executes one generation.
     *
     * @param dataObject    the data
     * @param population    the current population
     * @return              the resulting population
     */
    private PopulationType executeOne(DataObject dataObject, PopulationType population) {
        for (Actor actor : actorList) {
            actor.act(this, population);
        }

        population.setHierarchyPaths();

        feedInitialPopulation(dataObject, population);

        PopulationType newPopulation = execute(dataObject, population);

        this.gen++;

        return newPopulation;
    }

    /**
     * This method executes one generation.
     *
     * @param dataObject the data
     * @param population the current population
     * @return the resulting population
     */
    public abstract PopulationType execute(DataObject dataObject, PopulationType population);

    /**
     * This method feeds the data to the initial population.
     *
     * @param dataObject the data
     * @param population the initial population
     */
    public void feedInitialPopulation(DataObject dataObject, PopulationType population) {
        IndividualPopulation individuals = population.toIndividualPopulation();
        for (Individual individual : individuals) {
            individual.processData(dataObject);
        }
    }

    /**
     * Adds an actor.
     *
     * @param actor the actor
     */
    public void addActor(Actor actor) {
        this.actorList.add(actor);
    }

    /**
     * Gets the comparator operator.
     *
     * @return the comparator object
     */
    public Scoring getScoring() {
        return scoring;
    }

    /**
     * Gets the selection operator.
     *
     * @return the selection object
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * Gets the crossover operator.
     *
     * @return the crossover object
     */
    public Crossover getCrossover() {
        return crossover;
    }

    /**
     * Gets the simpleMutation operator.
     *
     * @return the simpleMutation object
     */
    public Mutation getMutation() {
        return mutation;
    }

    /**
     * Gets the solutionFoundFlag property.
     *
     * @return the boolean value
     */
    public boolean isSolutionFoundFlag() {
        return solutionFoundFlag;
    }

    /**
     * Sets the solutionFoundFlag property.
     *
     * @param solutionFoundFlag the new value
     */
    public void setSolutionFoundFlag(boolean solutionFoundFlag) {
        this.solutionFoundFlag = solutionFoundFlag;
    }

    /**
     * Gets the current generation.
     *
     * @return the generation
     */
    public int getGen() {
        return gen;
    }

    /**
     * Gets the current stage.
     * @return the stage
     */
    public String getStage() {
        return stage;
    }

    /**
     * Sets the current stage.
     * @param stage the new stage
     */
    public void setStage(String stage) {
        this.stage = stage;
    }

    /**
     * This method gets the size array of the initial population.
     *
     * @return the size array
     */
    public List<Integer> getSizeArray() {
        return sizeArray;
    }

    /**
     * This method gets the skip list of the environment.
     * This is used to skip certain sub populations.
     * This can be useful when for example a sub problem has been solved.
     *
     * @return the set containing the indices of the sup populations to be skipped
     */
    public Set<Integer> getSkipList() {
        return skipList;
    }
}
