package jga.operators.selection;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the roulette selection operator with lay back.
 */
public class RouletteWithLayBackSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation survivors = new IndividualPopulation();

        int totalScore = 0;

        for (Individual individual : population) {
            totalScore += individual.getScore();
        }

        while (survivors.size() < wantedPopulationSize) {
            int rouletteWheelChoice = RandomUtil.getInstance().nextInt(totalScore);
            int indexOfIndividual = findIndex(population, rouletteWheelChoice);
            Individual choice = population.get(indexOfIndividual).copy();

            survivors.add(choice);
        }

        return survivors;
    }

    private int findIndex(IndividualPopulation population, int rouletteWheelChoice) {
        int cumulativeRoulette = 0;
        for (int i = 0; i < population.size(); i++) {
            if (cumulativeRoulette > rouletteWheelChoice) {
                return i;
            }

            cumulativeRoulette += population.get(i).getScore();
        }
        return population.size() - 1;
    }
}
