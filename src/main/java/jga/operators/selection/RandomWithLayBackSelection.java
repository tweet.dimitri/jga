package jga.operators.selection;

import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the random selection operator with lay back.
 */
public class RandomWithLayBackSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation survivors = new IndividualPopulation();

        for (int i = 0; i < wantedPopulationSize; i++) {
            int index = RandomUtil.getInstance().nextInt(population.size());

            survivors.add(population.get(index).copy());
        }

        return survivors;
    }
}
