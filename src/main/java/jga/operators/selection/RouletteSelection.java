package jga.operators.selection;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the roulette selection operator without lay back.
 */
public class RouletteSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation survivors = new IndividualPopulation();

        IndividualPopulation copy = (IndividualPopulation) population.clone();

        int totalScore = 0;

        for (Individual individual : copy) {
            totalScore += individual.getScore();
        }

        while (survivors.size() < wantedPopulationSize) {
            int rouletteWheelChoice = RandomUtil.getInstance().nextInt(totalScore);
            int index = findIndex(copy, rouletteWheelChoice);
            Individual removed = copy.remove(index);

            totalScore -= removed.getScore();

            survivors.add(removed);
        }

        return survivors;
    }

    private int findIndex(IndividualPopulation population, int rouletteWheelChoice) {
        int cumulativeRoulette = 0;
        for (int i = 0; i < population.size(); i++) {
            if (cumulativeRoulette > rouletteWheelChoice) {
                return i;
            }

            cumulativeRoulette += population.get(i).getScore();
        }
        return population.size() - 1;
    }
}
