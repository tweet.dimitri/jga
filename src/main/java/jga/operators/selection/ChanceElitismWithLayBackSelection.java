package jga.operators.selection;

import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the chance elitism selection operator with lay back.
 */
public class ChanceElitismWithLayBackSelection implements Selection {


    // TODO this one is faulty
    // it fucks up the process data from individuals for some reason

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation survivingPopulation = new IndividualPopulation();

        while (survivingPopulation.size() < wantedPopulationSize) {
            for (int i = 0; i < population.size(); i++) {
                if (survivingPopulation.size() >= wantedPopulationSize) {
                    break;
                }

                if (RandomUtil.getInstance().nextDouble() > ((double) i) / population.size()) {
                    survivingPopulation.add(population.get(i).copy());
                }
            }
        }

        return survivingPopulation;
    }
}
