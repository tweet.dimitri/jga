package jga.operators.selection;

import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the random selection operator.
 */
public class RandomSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation copy = (IndividualPopulation) population.clone();

        IndividualPopulation survivors = new IndividualPopulation();

        for (int i = 0; i < wantedPopulationSize; i++) {
            int index = RandomUtil.getInstance().nextInt(copy.size());

            survivors.add(copy.remove(index));
        }

        return survivors;
    }
}
