package jga.operators.selection;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the tournament selection operator.
 */
public class TournamentSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation copy = (IndividualPopulation) population.clone();

        int tournamentSize = population.size() / wantedPopulationSize;

        IndividualPopulation survivors = new IndividualPopulation();

        while (survivors.size() < wantedPopulationSize) {
            // if the copy population is empty just add un added individuals from the population
            if (copy.size() == 0) {
                addUntilEnough(population, survivors, wantedPopulationSize);
                break;
            }

            IndividualPopulation tournament = new IndividualPopulation();

            // gather the tournament participants
            while (tournament.size() < tournamentSize) {
                int index = RandomUtil.getInstance().nextInt(copy.size());

                tournament.add(copy.remove(index));
            }

            survivors.add(doTournament(tournament));
        }

        return survivors;
    }

    private void addUntilEnough(IndividualPopulation population,
                                IndividualPopulation survivors,
                                int maxSize) {
        for (Individual individual : population) {
            if (survivors.size() == maxSize) {
                break;
            }

            if (!survivors.contains(individual)) {
                survivors.add(individual);
            }
        }
    }

    private Individual doTournament(IndividualPopulation tournament) {
        int bestScore = Integer.MIN_VALUE;
        Individual tournamentWinner = null;

        for (Individual participant : tournament) {
            if (bestScore < participant.getScore()) {
                bestScore = participant.getScore();
                tournamentWinner = participant;
            }
        }

        if (tournamentWinner == null) {
            throw new IllegalStateException("Something weird happend!");
        }

        return tournamentWinner;
    }
}
