package jga.operators.selection;

import jga.populations.IndividualPopulation;

/**
 * This interface describes the selection operator.
 */
public interface Selection {

    /**
     * This method should select a certain group.
     * It must receive a sorted population in order to work properly
     *
     * @param population the current sorted population
     * @param wantedSize the wanted population size
     * @return the selected population
     */
    IndividualPopulation select(IndividualPopulation population, int wantedSize);
}
