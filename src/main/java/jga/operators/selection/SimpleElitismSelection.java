package jga.operators.selection;

import jga.populations.IndividualPopulation;

/**
 * This class implements the simple elitism selection operator.
 */
public class SimpleElitismSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation survivors = new IndividualPopulation();

        for (int i = 0; i < wantedPopulationSize; i++) {
            survivors.add(population.get(i));
        }

        return survivors;
    }
}
