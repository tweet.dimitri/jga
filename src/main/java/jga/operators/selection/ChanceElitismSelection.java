package jga.operators.selection;

import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the chance elitism selection operator without lay back.
 */
public class ChanceElitismSelection implements Selection {

    @Override
    public IndividualPopulation select(IndividualPopulation population, int wantedPopulationSize) {
        if (population == null) {
            throw new IllegalArgumentException("Population is null!");
        }

        if (wantedPopulationSize > population.size()) {
            throw new IllegalArgumentException("Population is smaller then wanted size.");
        }

        IndividualPopulation copy = (IndividualPopulation) population.clone();

        IndividualPopulation survivingPopulation = new IndividualPopulation();

        while (survivingPopulation.size() < wantedPopulationSize) {
            for (int i = 0; i < copy.size(); i++) {
                if (survivingPopulation.size() >= wantedPopulationSize) {
                    break;
                }

                if (RandomUtil.getInstance().nextDouble() > ((double) i) / copy.size()) {
                    survivingPopulation.add(copy.remove(i));
                }
            }
        }

        return survivingPopulation;
    }
}
