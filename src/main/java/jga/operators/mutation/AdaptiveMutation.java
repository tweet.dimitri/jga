package jga.operators.mutation;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the adaptive mutation operator.
 */
public class AdaptiveMutation extends Mutation {

    private double mutationsPerIndividual;

    /**
     * Constructor for the mutation operator.
     * This operator works by picking random individuals.
     * The amount of individuals will depend on the mutation rate parameter.
     * The type of mutation will depend on the additional rate parameters.
     * Although there are 4 types of mutations implemented,
     * there are only 3 parameters for them.
     * This means that the final mutation type has a chance of 1 - (param1 + param2 + param3).
     *
     * @param mutationsPerIndividual the wanted amount of mutations per individual
     * @param addMutationChance      the chance of the add mutation
     * @param removeMutationChance   the chance of the remove mutation
     * @param pointMutationChance    the chance of the point mutation
     */
    public AdaptiveMutation(int mutationsPerIndividual,
                            double addMutationChance,
                            double removeMutationChance,
                            double pointMutationChance) {
        super(0, addMutationChance, removeMutationChance, pointMutationChance);
        this.mutationsPerIndividual = mutationsPerIndividual;
    }

    @Override
    public void mutate(IndividualPopulation individuals) {
        for (Individual individual : individuals) {
            mutateIndividual(individual);
        }
    }

    /**
     * This method mutates one individual.
     *
     * @param individual the individual to mutate
     */
    protected void mutateIndividual(Individual individual) {
        setMutationRate(mutationsPerIndividual / individual.getDNALength());
        for (int i = 0; i < individual.getDNALength() + 1; i++) {
            double doMutate = RandomUtil.getInstance().nextDouble();

            if (doMutate > getMutationRate()) {
                continue;
            }

            double choice = RandomUtil.getInstance().nextDouble();
            if (choice < getAddMutationChance()) {
                individual.addMutation(i);
            } else if (choice < getAddMutationChance() + getRemoveMutationChance()) {
                individual.removeMutation(i);
            } else if (choice < getAddMutationChance()
                    + getRemoveMutationChance()
                    + getPointMutationChance()) {
                individual.pointMutation(i);
            } else {
                individual.deltaMutation(i);
            }
        }
    }

    /**
     * Gets the mutations per individual.
     *
     * @return the amount
     */
    public double getMutationsPerIndividual() {
        return mutationsPerIndividual;
    }

    /**
     * Sets the mutations per individual.
     *
     * @param mutationsPerIndividual the new amount
     */
    public void setMutationsPerIndividual(double mutationsPerIndividual) {
        this.mutationsPerIndividual = mutationsPerIndividual;
    }
}
