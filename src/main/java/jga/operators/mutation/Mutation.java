package jga.operators.mutation;

import jga.populations.IndividualPopulation;

/**
 * This abstract class describes the mutation operator.
 */
public abstract class Mutation {

    private double mutationRate;
    private double addMutationChance;
    private double removeMutationChance;
    private double pointMutationChance;
    private double deltaMutationChance;

    /**
     * Constructor for the mutation operator.
     * This operator works by picking random individuals.
     * The amount of individuals will depend on the mutation rate parameter.
     * The type of mutation will depend on the additional rate parameters.
     * Although there are 4 types of mutations implemented,
     * there are only 3 parameters for them.
     * This means that the final mutation type has a chance of 1 - (param1 + param2 + param3).
     *
     * @param mutationRate         the mutation rate
     * @param addMutationChance    the chance of the add mutation
     * @param removeMutationChance the chance of the remove mutation
     * @param pointMutationChance  the chance of the point mutation
     */
    public Mutation(double mutationRate,
                    double addMutationChance,
                    double removeMutationChance,
                    double pointMutationChance) {
        if (addMutationChance + removeMutationChance + pointMutationChance > 1) {
            throw new IllegalArgumentException("Chances can't be bigger than one summed!");
        }

        this.mutationRate = mutationRate;
        this.addMutationChance = addMutationChance;
        this.removeMutationChance = removeMutationChance;
        this.pointMutationChance = pointMutationChance;
        this.deltaMutationChance = 1 - (addMutationChance
                + removeMutationChance
                + pointMutationChance);
    }

    /**
     * The mutate method which will mutate some individuals.
     *
     * @param individuals the population as individuals
     */
    public abstract void mutate(IndividualPopulation individuals);

    /**
     * Gets the mutation rate.
     *
     * @return the mutation rate
     */
    public double getMutationRate() {
        return mutationRate;
    }

    /**
     * Sets the mutation rate.
     *
     * @param mutationRate the new rate
     */
    public void setMutationRate(double mutationRate) {
        this.mutationRate = mutationRate;
    }

    /**
     * Gets the add chance.
     *
     * @return the add chance
     */
    public double getAddMutationChance() {
        return addMutationChance;
    }

    /**
     * Sets the add chance.
     *
     * @param chance the new chance
     */
    public void setAddMutationChance(double chance) {
        this.addMutationChance = chance;
    }

    /**
     * Gets the remove chance.
     *
     * @return the remove chance
     */
    public double getRemoveMutationChance() {
        return removeMutationChance;
    }

    /**
     * Sets the remove chance.
     *
     * @param chance the new chance
     */
    public void setRemoveMutationChance(double chance) {
        this.removeMutationChance = chance;
    }

    /**
     * Gets the point chance.
     *
     * @return the point chance
     */
    public double getPointMutationChance() {
        return pointMutationChance;
    }

    /**
     * Sets the point chance.
     *
     * @param chance the new chance
     */
    public void setPointMutationChance(double chance) {
        this.pointMutationChance = chance;
    }

    /**
     * Gets the delta chance.
     *
     * @return the delta chance
     */
    public double getDeltaMutationChance() {
        return deltaMutationChance;
    }

    /**
     * Sets the delta chance.
     *
     * @param chance the new chance
     */
    public void setDeltaMutationChance(double chance) {
        this.deltaMutationChance = chance;
    }
}
