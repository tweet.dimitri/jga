package jga.operators.mutation;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;

/**
 * This class implements the adaptive mutation operator.
 */
public class AdaptiveFitnessBasedMutation extends AdaptiveMutation {

    private int hierarchyPathIndex = -1;

    private double fitnessThreshHold;

    /**
     * Constructor for the mutation operator.
     * This operator works by picking random individuals.
     * The amount of individuals will depend on the mutation rate parameter.
     * The type of mutation will depend on the additional rate parameters.
     * Although there are 4 types of mutations implemented,
     * there are only 3 parameters for them.
     * This means that the final mutation type has a chance of 1 - (param1 + param2 + param3).
     *
     * @param mutationsPerIndividual the wanted amount of mutations per individual
     */
    public AdaptiveFitnessBasedMutation(int mutationsPerIndividual) {
        super(mutationsPerIndividual, 0, 0, 0);
        final double standardThreshHold = 0.5d;
        this.fitnessThreshHold = standardThreshHold;
    }

    @Override
    public void mutate(IndividualPopulation individuals) {

        for (Individual individual : individuals) {
            int index = 0;
            if (hierarchyPathIndex != -1) {
                index = individual.getHierarchyPath()[hierarchyPathIndex];
            }



            final double quarter = 0.25d;
            final double smallChance = 0.1d;
            final double bigChance = 0.4d;

            if (individuals.getBestIndividual() == null || individuals.getBestIndividual().getFitness() == null) {
                setAddMutationChance(quarter);
                setRemoveMutationChance(quarter);
                setPointMutationChance(quarter);
                setDeltaMutationChance(quarter);
            } else if (individuals.getBestIndividual().getFitness()[index] < fitnessThreshHold) {
                setAddMutationChance(quarter);
                setRemoveMutationChance(quarter);
                setPointMutationChance(quarter);
                setDeltaMutationChance(quarter);
            } else {
                setAddMutationChance(bigChance);
                setRemoveMutationChance(bigChance);
                setPointMutationChance(smallChance);
                setDeltaMutationChance(smallChance);
            }

            mutateIndividual(individual);
        }

    }

    /**
     * This method gets the hierarchy path index.
     *
     * @return the hierarchy path index
     */
    public int getHierarchyPathIndex() {
        return hierarchyPathIndex;
    }

    /**
     * This method sets the hierarchy path index.
     *
     * @param hierarchyPathIndex the new index
     */
    public void setHierarchyPathIndex(int hierarchyPathIndex) {
        this.hierarchyPathIndex = hierarchyPathIndex;
    }

    /**
     * This method gets the fitness thresh hold.
     *
     * @return the fitness thresh hold
     */
    public double getFitnessThreshold() {
        return fitnessThreshHold;
    }

    /**
     * This method sets the fitness thresh hold.
     *
     * @param fitnessThreshHold the new value
     */
    public void setFitnessThreshold(double fitnessThreshHold) {
        this.fitnessThreshHold = fitnessThreshHold;
    }
}
