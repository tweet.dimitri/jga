package jga.operators.mutation;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the simple mutation operator.
 */
public class SimpleMutation extends Mutation {

    /**
     * Constructor for the mutation operator.
     * This operator works by picking random individuals.
     * The amount of individuals will depend on the mutation rate parameter.
     * The type of mutation will depend on the additional rate parameters.
     * Although there are 4 types of mutations implemented,
     * there are only 3 parameters for them.
     * This means that the final mutation type has a chance of 1 - (param1 + param2 + param3).
     *
     * @param mutationRate         the mutation rate
     * @param addMutationChance    the chance of the add mutation
     * @param removeMutationChance the chance of the remove mutation
     * @param pointMutationChance  the chance of the point mutation
     */
    public SimpleMutation(double mutationRate,
                          double addMutationChance,
                          double removeMutationChance,
                          double pointMutationChance) {
        super(mutationRate, addMutationChance, removeMutationChance, pointMutationChance);
    }

    @Override
    public void mutate(IndividualPopulation individuals) {
        for (Individual individual : individuals) {
            for (int i = 0; i < individual.getDNALength() + 1; i++) {
                double doMutate = RandomUtil.getInstance().nextDouble();

                if (doMutate > getMutationRate()) {
                    continue;
                }

                double choice = RandomUtil.getInstance().nextDouble();
                if (choice < getAddMutationChance()) {
                    individual.addMutation(i);
                } else if (choice < getAddMutationChance() + getRemoveMutationChance()) {
                    individual.removeMutation(i);
                } else if (choice < getAddMutationChance()
                        + getRemoveMutationChance()
                        + getPointMutationChance()) {
                    individual.pointMutation(i);
                } else {
                    individual.deltaMutation(i);
                }
            }
        }
    }
}
