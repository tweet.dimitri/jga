package jga.operators.scoring.comparing;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;

/**
 * This class implements the multi objective compare operator. this basically is NSGA-II i think?
 * not entirely we do not score them the right way
 */
public class MultiObjectiveComparing extends Comparing {

    // TODO test this

    @Override
    public void compare(IndividualPopulation individuals) {
        int[] dominated = new int[individuals.size()];

        // loop through the individuals to determine the dominated value
        for (int i = 0; i < individuals.size(); i++) {
            Individual currentIndividual = individuals.get(i);
            if (currentIndividual.getFitness() == null) {
                throw new IllegalStateException("Individuals can only be sorted after processing!");
            }

            dominated[i] = getDominatedValue(individuals, currentIndividual);
        }

        // set the score temporary
        for (int i = 0; i < individuals.size(); i++) {
            individuals.get(i).setScore(dominated[i]);
        }
    }

    private int getDominatedValue(IndividualPopulation individuals, Individual individual) {
        double[] currentFitness = individual.getFitness();

        int dominated = 0;

        // loop through the individuals to check if one or more dominated the current individual
        for (Individual comparedIndividual : individuals) {
            if (comparedIndividual.getFitness() == null) {
                throw new IllegalStateException("Individuals need to processing be sorted after !");
            }

            if (individual == comparedIndividual) {
                continue;
            }
            double[] comparedFitness = comparedIndividual.getFitness();
            boolean doesDominate = true;

            // loop through both their fitness arrays
            // to check if the compared individual
            // dominates the current individual on all fields
            for (int k = 0; k < currentFitness.length; k++) {
                if (currentFitness[k] > comparedFitness[k]) {
                    doesDominate = false;
                    break;
                }
            }

            // if the compared individual dominates the current individual
            // then increase the dominated value by one
            if (doesDominate) {
                dominated++;
            }
        }
        return dominated;
    }
}
