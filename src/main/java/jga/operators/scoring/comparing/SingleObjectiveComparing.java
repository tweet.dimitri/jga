package jga.operators.scoring.comparing;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;

/**
 * This class implements the single objective compare operator.
 */
public class SingleObjectiveComparing extends Comparing {

    private int hierarchyPathIndex = -1;

    @Override
    public void compare(IndividualPopulation individuals) {
        if (hierarchyPathIndex != -1) {
            for (Individual individual : individuals) {
                doChecks(individual);
            }

            individuals.sort((o1, o2) -> Double.compare(
                    o2.getFitness()[o2.getHierarchyPath()[hierarchyPathIndex]],
                    o1.getFitness()[o1.getHierarchyPath()[hierarchyPathIndex]]));
        } else {
            individuals.sort((o1, o2) -> Double.compare(
                    o2.getFitness()[0],
                    o1.getFitness()[0]));
        }

        // if path[hierarchyPathIndex] != individualPath[hierarchyPathIndex]
        // then this means we are comparing different islands or families
        // TODO log this

        for (int i = 0; i < individuals.size(); i++) {
            individuals.get(i).setScore(i);
        }
    }

    /**
     * This method checks the hierarchy path of the individual.
     *
     * @param individual the individual to check
     */
    private void doChecks(Individual individual) {
        int[] path = individual.getHierarchyPath();
        if (path.length == 0) {
            throw new IllegalStateException("Can't check hierarchy path because it is missing!");
        }
        if (path.length <= hierarchyPathIndex) {
            throw new IllegalStateException("Can't check hierarchy path because it is to short!");
        }
        if (individual.getFitness().length <= path[hierarchyPathIndex]) {
            throw new IllegalStateException("Can't check hierarchy path because the fitness array is to short!");
        }
    }

    /**
     * This method gets the hierarchy path index.
     *
     * @return the hierarchy path index
     */
    public int getHierarchyPathIndex() {
        return hierarchyPathIndex;
    }

    /**
     * This method sets the hierarchy path index.
     *
     * @param hierarchyPathIndex the new index
     */
    public void setHierarchyPathIndex(int hierarchyPathIndex) {
        this.hierarchyPathIndex = hierarchyPathIndex;
    }

}
