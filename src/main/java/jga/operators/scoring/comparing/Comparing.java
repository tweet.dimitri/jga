package jga.operators.scoring.comparing;

import jga.populations.IndividualPopulation;

/**
 * This abstract class describes the compare operator.
 */
public abstract class Comparing {

    /**
     * This method should compare the population based on their fitness array.
     *
     * @param individuals the population as individuals
     */
    public abstract void compare(IndividualPopulation individuals);
}
