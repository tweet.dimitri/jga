package jga.operators.scoring;

import jga.operators.scoring.comparing.Comparing;
import jga.populations.IndividualPopulation;

/**
 * This abstract class describes the scoring operator.
 */
public abstract class Scoring {

    private Comparing comparing;

    /**
     * Constructor for the scoring operator.
     *
     * @param comparing the comparing operator
     */
    public Scoring(Comparing comparing) {
        this.comparing = comparing;
    }

    /**
     * This method should set the scores of the individuals.
     *
     * @param individuals the population as individuals
     */
    public abstract void setScoresAndSort(IndividualPopulation individuals);

    /**
     * Gets the comparator operator.
     * @return the operator
     */
    public Comparing getComparing() {
        return comparing;
    }
}
