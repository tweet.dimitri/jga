package jga.operators.scoring;

import jga.individuals.Individual;
import jga.operators.scoring.comparing.Comparing;
import jga.populations.IndividualPopulation;

import java.util.Comparator;

/**
 * This class implements the rank scoring operator.
 */
public class RankScoring extends Scoring {

    /**
     * Constructor for the scoring operator.
     *
     * @param comparing the comparing operator
     */
    public RankScoring(Comparing comparing) {
        super(comparing);
    }

    @Override
    public void setScoresAndSort(IndividualPopulation individuals) {
        getComparing().compare(individuals);

        individuals.sort(Comparator.comparingInt(Individual::getScore));
        individuals.setBestIndividual(individuals.get(0));
    }
}
