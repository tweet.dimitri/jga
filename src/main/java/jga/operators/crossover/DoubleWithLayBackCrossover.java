package jga.operators.crossover;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the simple cross over operator.
 */
public class DoubleWithLayBackCrossover implements Crossover {

    @Override
    public IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation offspring = new IndividualPopulation();

        while (offspring.size() + population.size() < wantedPopulationSize) {
            int indexParentOne = RandomUtil.getInstance().nextInt(population.size());
            Individual parentOne = population.get(indexParentOne);

            int indexParentTwo = RandomUtil.getInstance().nextInt(population.size());
            Individual parentTwo = population.get(indexParentTwo);

            int smallest = parentOne.getDNALength();

            if (parentTwo.getDNALength() < smallest) {
                smallest = parentTwo.getDNALength();
            }

            int crossoverPoint = RandomUtil.getInstance().nextInt(smallest);
            int crossoverPointTwo = RandomUtil.getInstance().nextInt(smallest);

            int firstPoint = crossoverPoint;
            int secondPoint = crossoverPointTwo;

            if (crossoverPoint > crossoverPointTwo) {
                firstPoint = crossoverPointTwo;
                secondPoint = crossoverPoint;
            }

            Individual child = parentOne.crossover(firstPoint, parentTwo);

            Individual doubleCrossoverParent =  parentOne;

            if (RandomUtil.getInstance().nextBoolean()) {
                doubleCrossoverParent = parentTwo;
            }

            offspring.add(child.crossover(secondPoint, doubleCrossoverParent));
        }
        return offspring;
    }
}
