package jga.operators.crossover;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the double crossover operator .
 */
public class DoubleCrossover implements Crossover {

    @Override
    public IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation offspring = new IndividualPopulation();

        int childrenPerPair = (int) Math.ceil(((double) wantedPopulationSize) / population.size());

        IndividualPopulation copyPopulation = (IndividualPopulation) population.clone();

        while (offspring.size() + population.size() < wantedPopulationSize) {
            doCrossover(copyPopulation, offspring, childrenPerPair);
        }

        while (offspring.size() + population.size() > wantedPopulationSize) {
            offspring.remove(0);
        }

        return offspring;
    }

    private void doCrossover(IndividualPopulation population,
                             IndividualPopulation offspring,
                             int childrenPerPair) {
        int indexParentOne = RandomUtil.getInstance().nextInt(population.size());
        Individual parentOne = population.remove(indexParentOne);

        int indexParentTwo = RandomUtil.getInstance().nextInt(population.size());
        Individual parentTwo = population.remove(indexParentTwo);

        int smallest = parentOne.getDNALength();

        if (parentTwo.getDNALength() < smallest) {
            smallest = parentTwo.getDNALength();
        }

        for (int i = 0; i < childrenPerPair; i++) {
            int crossoverPoint = RandomUtil.getInstance().nextInt(smallest);
            int crossoverPointTwo = RandomUtil.getInstance().nextInt(smallest);

            int firstPoint = crossoverPoint;
            int secondPoint = crossoverPointTwo;

            if (crossoverPoint > crossoverPointTwo) {
                firstPoint = crossoverPointTwo;
                secondPoint = crossoverPoint;
            }

            Individual child = parentOne.crossover(firstPoint, parentTwo);

            Individual doubleCrossoverParent = parentOne;

            if (RandomUtil.getInstance().nextBoolean()) {
                doubleCrossoverParent = parentTwo;
            }

            offspring.add(child.crossover(secondPoint, doubleCrossoverParent));
        }
    }
    // TODO make this into multicrossover or something
}
