package jga.operators.crossover;

import jga.populations.IndividualPopulation;

/**
 * This class implements the asexual cross over operator.
 */
public class AsexualCrossover implements Crossover {

    @Override
    public IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation offspring = new IndividualPopulation();

        while (offspring.size() + population.size() < wantedPopulationSize) {
            for (int i = 0; i < population.size(); i++) {
                if (offspring.size() + population.size() >= wantedPopulationSize) {
                    break;
                }

                offspring.add(population.get(i).copy());
            }
        }

        return offspring;
    }
}
