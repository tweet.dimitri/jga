package jga.operators.crossover;

import jga.populations.IndividualPopulation;

/**
 * This interface describes the cross over operator.
 */
public interface Crossover {

    /**
     * This method should pick the parents out of the given population
     * to create the offspring which is then returned.
     *
     * @param population           the current population
     * @param wantedPopulationSize the original population size
     * @return the offspring
     */
    IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize);
}
