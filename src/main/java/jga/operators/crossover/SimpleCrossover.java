package jga.operators.crossover;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the simple cross over operator.
 */
public class SimpleCrossover implements Crossover {

    @Override
    public IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation offspring = new IndividualPopulation();

        int childrenPerPair = (int) Math.ceil(((double) wantedPopulationSize) / population.size());

        IndividualPopulation copyPopulation = (IndividualPopulation) population.clone();

        while (offspring.size() + population.size() < wantedPopulationSize) {
            if (copyPopulation.size() < 2) {
                break;
            }

            int indexParentOne = RandomUtil.getInstance().nextInt(copyPopulation.size());
            Individual parentOne = copyPopulation.remove(indexParentOne);

            int indexParentTwo = RandomUtil.getInstance().nextInt(copyPopulation.size());
            Individual parentTwo = copyPopulation.remove(indexParentTwo);

            int smallest = parentOne.getDNALength();

            if (parentTwo.getDNALength() < smallest) {
                smallest = parentTwo.getDNALength();
            }

            for (int i = 0; i < childrenPerPair; i++) {
                int crossoverPoint = RandomUtil.getInstance().nextInt(smallest);

                offspring.add(parentOne.crossover(crossoverPoint, parentTwo));
            }
        }

        return offspring;
    }
}

