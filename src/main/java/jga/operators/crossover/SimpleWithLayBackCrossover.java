package jga.operators.crossover;

import jga.individuals.Individual;
import jga.populations.IndividualPopulation;
import jga.utils.RandomUtil;

/**
 * This class implements the simple cross over operator with lay back.
 */
public class SimpleWithLayBackCrossover implements Crossover {

    @Override
    public IndividualPopulation breed(IndividualPopulation population, int wantedPopulationSize) {
        IndividualPopulation offspring = new IndividualPopulation();

        IndividualPopulation copyPopulation = (IndividualPopulation) population.clone();

        while (offspring.size() + population.size() < wantedPopulationSize) {
            int indexParentOne = RandomUtil.getInstance().nextInt(copyPopulation.size());
            Individual parentOne = copyPopulation.get(indexParentOne);

            int indexParentTwo = RandomUtil.getInstance().nextInt(copyPopulation.size());
            Individual parentTwo = copyPopulation.get(indexParentTwo);

            int smallest = parentOne.getDNALength();

            if (parentTwo.getDNALength() < smallest) {
                smallest = parentTwo.getDNALength();
            }

            int crossoverPoint = RandomUtil.getInstance().nextInt(smallest);

            offspring.add(parentOne.crossover(crossoverPoint, parentTwo));
        }

        return offspring;
    }
}
