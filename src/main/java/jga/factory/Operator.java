package jga.factory;

/**
 * The operator types class.
 */
public class Operator {

    /**
     * The compare types.
     */
    public enum Compare {
        SINGLE_OBJECTIVE,
        MANY_OBJECTIVE
    }

    /**
     * The scoring types.
     */
    public enum Scoring {
        EXPONENTIAL_SCORING,
        PROPORTIONAL_SCORING,
        RANK_SCORING
    }

    /**
     * The cross over types.
     */
    public enum Crossover {
        ARITHMETIC_CROSSOVER,
        ASEXUAL_CROSSOVER,
        DOUBLE_CROSSOVER,
        DOUBLE_WITH_LAYBACK_CROSSOVER,
        SHUFFLE_CROSSOVER,
        SIMPLE_CROSSOVER,
        SIMPLE_WITH_LAYBACK_CROSSOVER,
        TOURNAMENT_CROSSOVER
    }

    /**
     * The selection types.
     */
    public enum Selection {
        CHANCE_ELITISM_SELECTION,
        CHANCE_ELITISM_WITH_LAYBACK_SELECTION,
        KILL_TOURNAMENT_SELECTION,
        RANDOM_SELECTION,
        RANDOM_WITH_LAYBACK_SELECTION,
        ROULETTE_SELECTION,
        ROULETTE_WITH_LAYBACK_SELECTION,
        SIMPLE_ELITISM_SELECTION,
        TOURNAMENT_SELECTION
    }

    /**
     * The mutation types.
     */
    public enum Mutation {
        SIMPLE_MUTATION,
        ADAPTIVE_MUTATION,
        ADAPTIVE_FITNESS_BASED_MUTATION
    }
}
