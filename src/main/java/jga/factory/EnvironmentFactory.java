package jga.factory;

import jga.environments.*;
import jga.operators.crossover.*;
import jga.operators.mutation.AdaptiveFitnessBasedMutation;
import jga.operators.mutation.AdaptiveMutation;
import jga.operators.mutation.Mutation;
import jga.operators.mutation.SimpleMutation;
import jga.operators.scoring.ExponentialScoring;
import jga.operators.scoring.ProportionalScoring;
import jga.operators.scoring.RankScoring;
import jga.operators.scoring.Scoring;
import jga.operators.scoring.comparing.Comparing;
import jga.operators.scoring.comparing.MultiObjectiveComparing;
import jga.operators.scoring.comparing.SingleObjectiveComparing;
import jga.operators.selection.*;


/**
 * This class implements the environment factory.
 * @param <DataObject> the type of data object
 */
public class EnvironmentFactory<DataObject> {

    /**
     * This method will create an environment based on the given enums.
     *
     * @param approach      the approach to use
     * @param comparing     the comparing operator to use
     * @param scoring       the scoring operator to use
     * @param crossover     the crossover operator to use
     * @param selection     the selection operator to use
     * @param mutation      the mutation operator to use
     * @return the created environment.
     */
    public Environment createEnvironment(Approach approach,
                                         Operator.Compare comparing,
                                         Operator.Scoring scoring,
                                         Operator.Crossover crossover,
                                         Operator.Selection selection,
                                         Operator.Mutation mutation) {
        Comparing comparingOperator = createComparing(comparing);
        Scoring scoringOperator = createScoring(scoring, comparingOperator);
        Crossover crossoverOperator = createCrossover(crossover);
        Selection selectionOperator = createSelection(selection);
        Mutation mutationOperator = createMutation(mutation);

        Environment environment;
        switch (approach) {
            case SIMPLE:
                environment = new SimpleEnvironment<DataObject>(
                        scoringOperator,
                        selectionOperator,
                        crossoverOperator,
                        mutationOperator);
                break;
            case MULTI:
                environment = new MultiEnvironment<DataObject>(
                        scoringOperator,
                        selectionOperator,
                        crossoverOperator,
                        mutationOperator);
                break;
            case MULTI_FAMILY:
                environment = new MultiMultiEnvironment(
                        scoringOperator,
                        selectionOperator,
                        crossoverOperator,
                        mutationOperator);
                break;
            default:
                throw new IllegalArgumentException("Invalid approach!");
        }
        return environment;
    }

    /**
     * This method creates the comparing operator based on the given enum.
     *
     * @param operator the operator enum
     * @return the created operator
     */
    private Comparing createComparing(Operator.Compare operator) {
        switch (operator) {
            case SINGLE_OBJECTIVE:
                return new SingleObjectiveComparing();
            case MANY_OBJECTIVE:
                return new MultiObjectiveComparing();
            default:
                throw new IllegalArgumentException("Invalid comparing operator!");
        }
    }

    /**
     * This method creates the scoring operator based on the given enum.
     *
     * @param operator the operator enum
     * @return the created operator
     */
    private Scoring createScoring(Operator.Scoring operator, Comparing comparing) {
        switch (operator) {
            case EXPONENTIAL_SCORING:
                return new ExponentialScoring(comparing);
            case PROPORTIONAL_SCORING:
                return new ProportionalScoring(comparing);
            case RANK_SCORING:
                return new RankScoring(comparing);
            default:
                throw new IllegalArgumentException("Invalid scoring operator!");
        }
    }

    /**
     * This method creates the crossover operator based on the given enum.
     *
     * @param operator the operator enum
     * @return the created operator
     */
    private Crossover createCrossover(Operator.Crossover operator) {
        switch (operator) {
            case ARITHMETIC_CROSSOVER:
                return new ArithmeticCrossover();
            case ASEXUAL_CROSSOVER:
                return new AsexualCrossover();
            case DOUBLE_CROSSOVER:
                return new DoubleCrossover();
            case DOUBLE_WITH_LAYBACK_CROSSOVER:
                return new DoubleWithLayBackCrossover();
            case SHUFFLE_CROSSOVER:
                return new ShuffleCrossover();
            case SIMPLE_CROSSOVER:
                return new SimpleCrossover();
            case SIMPLE_WITH_LAYBACK_CROSSOVER:
                return new SimpleWithLayBackCrossover();
            case TOURNAMENT_CROSSOVER:
                return new TournamentCrossover();
            default:
                throw new IllegalArgumentException("Invalid crossover operator!");
        }
    }

    /**
     * This method creates the selection operator based on the given enum.
     *
     * @param operator the operator enum
     * @return the created operator
     */
    private Selection createSelection(Operator.Selection operator) {
        switch (operator) {
            case CHANCE_ELITISM_SELECTION:
                return new ChanceElitismSelection();
            case CHANCE_ELITISM_WITH_LAYBACK_SELECTION:
                return new ChanceElitismWithLayBackSelection();
            case KILL_TOURNAMENT_SELECTION:
                return new KillTournamentSelection();
            case RANDOM_SELECTION:
                return new RandomSelection();
            case RANDOM_WITH_LAYBACK_SELECTION:
                return new RandomWithLayBackSelection();
            case ROULETTE_SELECTION:
                return new RouletteSelection();
            case ROULETTE_WITH_LAYBACK_SELECTION:
                return new RouletteWithLayBackSelection();
            case SIMPLE_ELITISM_SELECTION:
                return new SimpleElitismSelection();
            case TOURNAMENT_SELECTION:
                return new TournamentSelection();
            default:
                throw new IllegalArgumentException("Invalid selection operator!");
        }
    }

    /**
     * This method creates the mutation operator based on the given enum.
     *
     * @param operator the operator enum
     * @return the created operator
     */
    private Mutation createMutation(Operator.Mutation operator) {
        final double equalChance = 0.25d;
        final double mutationRate = 0.05d;
        switch (operator) {
            case SIMPLE_MUTATION:
                return new SimpleMutation(mutationRate, equalChance, equalChance, equalChance);
            case ADAPTIVE_MUTATION:
                return new AdaptiveMutation(1, equalChance, equalChance, equalChance);
            case ADAPTIVE_FITNESS_BASED_MUTATION:
                return new AdaptiveFitnessBasedMutation(1);
            default:
                throw new IllegalArgumentException("Invalid mutation operator!");
        }
    }
}
