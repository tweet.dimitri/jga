package jga.factory;

/**
 * The approach types.
 */
public enum Approach {
    MULTI_FAMILY, MULTI, SIMPLE
}
