package jga.problems;

import jga.individuals.Individual;


/**
 * This interface describes a problem to solve for a Genetic Algorithm.
 * @param <DNA> the type of dna the individuals of the Genetic Algorithm will have
 * @param <DataObject> the type of data the the Genetic Algorithm will deal with
 */
public interface Problem<DNA, DataObject> {

    /**
     * This method should process the new data and calculate the individuals fitness.
     *
     * @param individual    the individual that needs to be processed
     * @param dna           the dna
     * @param object        the data object
     * @return the calculated fitness array
     */
    double[] process(Individual individual, DNA dna, DataObject object);
}
